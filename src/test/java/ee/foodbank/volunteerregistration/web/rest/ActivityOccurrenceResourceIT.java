package ee.foodbank.volunteerregistration.web.rest;

import static ee.foodbank.volunteerregistration.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ee.foodbank.volunteerregistration.IntegrationTest;
import ee.foodbank.volunteerregistration.domain.Activity;
import ee.foodbank.volunteerregistration.domain.ActivityOccurrence;
import ee.foodbank.volunteerregistration.domain.Location;
import ee.foodbank.volunteerregistration.repository.ActivityOccurrenceRepository;
import ee.foodbank.volunteerregistration.service.dto.ActivityOccurrenceDTO;
import ee.foodbank.volunteerregistration.service.mapper.ActivityOccurrenceMapper;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ActivityOccurrenceResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ActivityOccurrenceResourceIT {

    private static final ZonedDateTime DEFAULT_START_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_START_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_END_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_END_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Integer DEFAULT_MAX_VOLUNTEERS = 1;
    private static final Integer UPDATED_MAX_VOLUNTEERS = 2;

    private static final Integer DEFAULT_REGISTERED_VOLUNTEERS = 0;
    private static final Integer UPDATED_REGISTERED_VOLUNTEERS = 1;

    @Autowired
    private ActivityOccurrenceRepository activityOccurrenceRepository;

    @Autowired
    private ActivityOccurrenceMapper activityOccurrenceMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restActivityOccurrenceMockMvc;

    private ActivityOccurrence activityOccurrence;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ActivityOccurrence createEntity(EntityManager em) {
        ActivityOccurrence activityOccurrence = new ActivityOccurrence()
            .startTime(DEFAULT_START_TIME)
            .endTime(DEFAULT_END_TIME)
            .maxVolunteers(DEFAULT_MAX_VOLUNTEERS)
            .registeredVolunteers(DEFAULT_REGISTERED_VOLUNTEERS);
        // Add required entity
        Activity activity;
        if (TestUtil.findAll(em, Activity.class).isEmpty()) {
            activity = ActivityResourceIT.createEntity(em);
            em.persist(activity);
            em.flush();
        } else {
            activity = TestUtil.findAll(em, Activity.class).get(0);
        }
        activityOccurrence.setActivity(activity);
        // Add required entity
        Location location;
        if (TestUtil.findAll(em, Location.class).isEmpty()) {
            location = LocationResourceIT.createEntity(em);
            em.persist(location);
            em.flush();
        } else {
            location = TestUtil.findAll(em, Location.class).get(0);
        }
        activityOccurrence.setLocation(location);
        return activityOccurrence;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ActivityOccurrence createUpdatedEntity(EntityManager em) {
        ActivityOccurrence activityOccurrence = new ActivityOccurrence()
            .startTime(UPDATED_START_TIME)
            .endTime(UPDATED_END_TIME)
            .maxVolunteers(UPDATED_MAX_VOLUNTEERS)
            .registeredVolunteers(UPDATED_REGISTERED_VOLUNTEERS);
        // Add required entity
        Activity activity;
        if (TestUtil.findAll(em, Activity.class).isEmpty()) {
            activity = ActivityResourceIT.createUpdatedEntity(em);
            em.persist(activity);
            em.flush();
        } else {
            activity = TestUtil.findAll(em, Activity.class).get(0);
        }
        activityOccurrence.setActivity(activity);
        // Add required entity
        Location location;
        if (TestUtil.findAll(em, Location.class).isEmpty()) {
            location = LocationResourceIT.createUpdatedEntity(em);
            em.persist(location);
            em.flush();
        } else {
            location = TestUtil.findAll(em, Location.class).get(0);
        }
        activityOccurrence.setLocation(location);
        return activityOccurrence;
    }

    @BeforeEach
    public void initTest() {
        activityOccurrence = createEntity(em);
    }

    @Test
    @Transactional
    void createActivityOccurrence() throws Exception {
        int databaseSizeBeforeCreate = activityOccurrenceRepository.findAll().size();
        // Create the ActivityOccurrence
        ActivityOccurrenceDTO activityOccurrenceDTO = activityOccurrenceMapper.toDto(activityOccurrence);
        restActivityOccurrenceMockMvc
            .perform(
                post("/api/activity-occurrences")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(activityOccurrenceDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ActivityOccurrence in the database
        List<ActivityOccurrence> activityOccurrenceList = activityOccurrenceRepository.findAll();
        assertThat(activityOccurrenceList).hasSize(databaseSizeBeforeCreate + 1);
        ActivityOccurrence testActivityOccurrence = activityOccurrenceList.get(activityOccurrenceList.size() - 1);
        assertThat(testActivityOccurrence.getStartTime()).isEqualTo(DEFAULT_START_TIME);
        assertThat(testActivityOccurrence.getEndTime()).isEqualTo(DEFAULT_END_TIME);
        assertThat(testActivityOccurrence.getMaxVolunteers()).isEqualTo(DEFAULT_MAX_VOLUNTEERS);
        assertThat(testActivityOccurrence.getRegisteredVolunteers()).isEqualTo(DEFAULT_REGISTERED_VOLUNTEERS);
    }

    @Test
    @Transactional
    void createActivityOccurrenceWithExistingId() throws Exception {
        // Create the ActivityOccurrence with an existing ID
        activityOccurrence.setId(1L);
        ActivityOccurrenceDTO activityOccurrenceDTO = activityOccurrenceMapper.toDto(activityOccurrence);

        int databaseSizeBeforeCreate = activityOccurrenceRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restActivityOccurrenceMockMvc
            .perform(
                post("/api/activity-occurrences")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(activityOccurrenceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ActivityOccurrence in the database
        List<ActivityOccurrence> activityOccurrenceList = activityOccurrenceRepository.findAll();
        assertThat(activityOccurrenceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkStartTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = activityOccurrenceRepository.findAll().size();
        // set the field null
        activityOccurrence.setStartTime(null);

        // Create the ActivityOccurrence, which fails.
        ActivityOccurrenceDTO activityOccurrenceDTO = activityOccurrenceMapper.toDto(activityOccurrence);

        restActivityOccurrenceMockMvc
            .perform(
                post("/api/activity-occurrences")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(activityOccurrenceDTO))
            )
            .andExpect(status().isBadRequest());

        List<ActivityOccurrence> activityOccurrenceList = activityOccurrenceRepository.findAll();
        assertThat(activityOccurrenceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEndTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = activityOccurrenceRepository.findAll().size();
        // set the field null
        activityOccurrence.setEndTime(null);

        // Create the ActivityOccurrence, which fails.
        ActivityOccurrenceDTO activityOccurrenceDTO = activityOccurrenceMapper.toDto(activityOccurrence);

        restActivityOccurrenceMockMvc
            .perform(
                post("/api/activity-occurrences")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(activityOccurrenceDTO))
            )
            .andExpect(status().isBadRequest());

        List<ActivityOccurrence> activityOccurrenceList = activityOccurrenceRepository.findAll();
        assertThat(activityOccurrenceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkMaxVolunteersIsRequired() throws Exception {
        int databaseSizeBeforeTest = activityOccurrenceRepository.findAll().size();
        // set the field null
        activityOccurrence.setMaxVolunteers(null);

        // Create the ActivityOccurrence, which fails.
        ActivityOccurrenceDTO activityOccurrenceDTO = activityOccurrenceMapper.toDto(activityOccurrence);

        restActivityOccurrenceMockMvc
            .perform(
                post("/api/activity-occurrences")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(activityOccurrenceDTO))
            )
            .andExpect(status().isBadRequest());

        List<ActivityOccurrence> activityOccurrenceList = activityOccurrenceRepository.findAll();
        assertThat(activityOccurrenceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRegisteredVolunteersIsRequired() throws Exception {
        int databaseSizeBeforeTest = activityOccurrenceRepository.findAll().size();
        // set the field null
        activityOccurrence.setRegisteredVolunteers(null);

        // Create the ActivityOccurrence, which fails.
        ActivityOccurrenceDTO activityOccurrenceDTO = activityOccurrenceMapper.toDto(activityOccurrence);

        restActivityOccurrenceMockMvc
            .perform(
                post("/api/activity-occurrences")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(activityOccurrenceDTO))
            )
            .andExpect(status().isBadRequest());

        List<ActivityOccurrence> activityOccurrenceList = activityOccurrenceRepository.findAll();
        assertThat(activityOccurrenceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllActivityOccurrences() throws Exception {
        // Initialize the database
        activityOccurrenceRepository.saveAndFlush(activityOccurrence);

        // Get all the activityOccurrenceList
        restActivityOccurrenceMockMvc
            .perform(get("/api/activity-occurrences?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(activityOccurrence.getId().intValue())))
            .andExpect(jsonPath("$.[*].startTime").value(hasItem(sameInstant(DEFAULT_START_TIME))))
            .andExpect(jsonPath("$.[*].endTime").value(hasItem(sameInstant(DEFAULT_END_TIME))))
            .andExpect(jsonPath("$.[*].maxVolunteers").value(hasItem(DEFAULT_MAX_VOLUNTEERS)))
            .andExpect(jsonPath("$.[*].registeredVolunteers").value(hasItem(DEFAULT_REGISTERED_VOLUNTEERS)));
    }

    @Test
    @Transactional
    void getActivityOccurrence() throws Exception {
        // Initialize the database
        activityOccurrenceRepository.saveAndFlush(activityOccurrence);

        // Get the activityOccurrence
        restActivityOccurrenceMockMvc
            .perform(get("/api/activity-occurrences/{id}", activityOccurrence.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(activityOccurrence.getId().intValue()))
            .andExpect(jsonPath("$.startTime").value(sameInstant(DEFAULT_START_TIME)))
            .andExpect(jsonPath("$.endTime").value(sameInstant(DEFAULT_END_TIME)))
            .andExpect(jsonPath("$.maxVolunteers").value(DEFAULT_MAX_VOLUNTEERS))
            .andExpect(jsonPath("$.registeredVolunteers").value(DEFAULT_REGISTERED_VOLUNTEERS));
    }

    @Test
    @Transactional
    void getNonExistingActivityOccurrence() throws Exception {
        // Get the activityOccurrence
        restActivityOccurrenceMockMvc.perform(get("/api/activity-occurrences/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void updateActivityOccurrence() throws Exception {
        // Initialize the database
        activityOccurrenceRepository.saveAndFlush(activityOccurrence);

        int databaseSizeBeforeUpdate = activityOccurrenceRepository.findAll().size();

        // Update the activityOccurrence
        ActivityOccurrence updatedActivityOccurrence = activityOccurrenceRepository.findById(activityOccurrence.getId()).get();
        // Disconnect from session so that the updates on updatedActivityOccurrence are not directly saved in db
        em.detach(updatedActivityOccurrence);
        updatedActivityOccurrence
            .startTime(UPDATED_START_TIME)
            .endTime(UPDATED_END_TIME)
            .maxVolunteers(UPDATED_MAX_VOLUNTEERS)
            .registeredVolunteers(UPDATED_REGISTERED_VOLUNTEERS);
        ActivityOccurrenceDTO activityOccurrenceDTO = activityOccurrenceMapper.toDto(updatedActivityOccurrence);

        restActivityOccurrenceMockMvc
            .perform(
                put("/api/activity-occurrences")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(activityOccurrenceDTO))
            )
            .andExpect(status().isOk());

        // Validate the ActivityOccurrence in the database
        List<ActivityOccurrence> activityOccurrenceList = activityOccurrenceRepository.findAll();
        assertThat(activityOccurrenceList).hasSize(databaseSizeBeforeUpdate);
        ActivityOccurrence testActivityOccurrence = activityOccurrenceList.get(activityOccurrenceList.size() - 1);
        assertThat(testActivityOccurrence.getStartTime()).isEqualTo(UPDATED_START_TIME);
        assertThat(testActivityOccurrence.getEndTime()).isEqualTo(UPDATED_END_TIME);
        assertThat(testActivityOccurrence.getMaxVolunteers()).isEqualTo(UPDATED_MAX_VOLUNTEERS);
        assertThat(testActivityOccurrence.getRegisteredVolunteers()).isEqualTo(UPDATED_REGISTERED_VOLUNTEERS);
    }

    @Test
    @Transactional
    void updateNonExistingActivityOccurrence() throws Exception {
        int databaseSizeBeforeUpdate = activityOccurrenceRepository.findAll().size();

        // Create the ActivityOccurrence
        ActivityOccurrenceDTO activityOccurrenceDTO = activityOccurrenceMapper.toDto(activityOccurrence);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restActivityOccurrenceMockMvc
            .perform(
                put("/api/activity-occurrences")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(activityOccurrenceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ActivityOccurrence in the database
        List<ActivityOccurrence> activityOccurrenceList = activityOccurrenceRepository.findAll();
        assertThat(activityOccurrenceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateActivityOccurrenceWithPatch() throws Exception {
        // Initialize the database
        activityOccurrenceRepository.saveAndFlush(activityOccurrence);

        int databaseSizeBeforeUpdate = activityOccurrenceRepository.findAll().size();

        // Update the activityOccurrence using partial update
        ActivityOccurrence partialUpdatedActivityOccurrence = new ActivityOccurrence();
        partialUpdatedActivityOccurrence.setId(activityOccurrence.getId());

        partialUpdatedActivityOccurrence.startTime(UPDATED_START_TIME).endTime(UPDATED_END_TIME);

        restActivityOccurrenceMockMvc
            .perform(
                patch("/api/activity-occurrences")
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedActivityOccurrence))
            )
            .andExpect(status().isOk());

        // Validate the ActivityOccurrence in the database
        List<ActivityOccurrence> activityOccurrenceList = activityOccurrenceRepository.findAll();
        assertThat(activityOccurrenceList).hasSize(databaseSizeBeforeUpdate);
        ActivityOccurrence testActivityOccurrence = activityOccurrenceList.get(activityOccurrenceList.size() - 1);
        assertThat(testActivityOccurrence.getStartTime()).isEqualTo(UPDATED_START_TIME);
        assertThat(testActivityOccurrence.getEndTime()).isEqualTo(UPDATED_END_TIME);
        assertThat(testActivityOccurrence.getMaxVolunteers()).isEqualTo(DEFAULT_MAX_VOLUNTEERS);
        assertThat(testActivityOccurrence.getRegisteredVolunteers()).isEqualTo(DEFAULT_REGISTERED_VOLUNTEERS);
    }

    @Test
    @Transactional
    void fullUpdateActivityOccurrenceWithPatch() throws Exception {
        // Initialize the database
        activityOccurrenceRepository.saveAndFlush(activityOccurrence);

        int databaseSizeBeforeUpdate = activityOccurrenceRepository.findAll().size();

        // Update the activityOccurrence using partial update
        ActivityOccurrence partialUpdatedActivityOccurrence = new ActivityOccurrence();
        partialUpdatedActivityOccurrence.setId(activityOccurrence.getId());

        partialUpdatedActivityOccurrence
            .startTime(UPDATED_START_TIME)
            .endTime(UPDATED_END_TIME)
            .maxVolunteers(UPDATED_MAX_VOLUNTEERS)
            .registeredVolunteers(UPDATED_REGISTERED_VOLUNTEERS);

        restActivityOccurrenceMockMvc
            .perform(
                patch("/api/activity-occurrences")
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedActivityOccurrence))
            )
            .andExpect(status().isOk());

        // Validate the ActivityOccurrence in the database
        List<ActivityOccurrence> activityOccurrenceList = activityOccurrenceRepository.findAll();
        assertThat(activityOccurrenceList).hasSize(databaseSizeBeforeUpdate);
        ActivityOccurrence testActivityOccurrence = activityOccurrenceList.get(activityOccurrenceList.size() - 1);
        assertThat(testActivityOccurrence.getStartTime()).isEqualTo(UPDATED_START_TIME);
        assertThat(testActivityOccurrence.getEndTime()).isEqualTo(UPDATED_END_TIME);
        assertThat(testActivityOccurrence.getMaxVolunteers()).isEqualTo(UPDATED_MAX_VOLUNTEERS);
        assertThat(testActivityOccurrence.getRegisteredVolunteers()).isEqualTo(UPDATED_REGISTERED_VOLUNTEERS);
    }

    @Test
    @Transactional
    void partialUpdateActivityOccurrenceShouldThrown() throws Exception {
        // Update the activityOccurrence without id should throw
        ActivityOccurrence partialUpdatedActivityOccurrence = new ActivityOccurrence();

        restActivityOccurrenceMockMvc
            .perform(
                patch("/api/activity-occurrences")
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedActivityOccurrence))
            )
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    void deleteActivityOccurrence() throws Exception {
        // Initialize the database
        activityOccurrenceRepository.saveAndFlush(activityOccurrence);

        int databaseSizeBeforeDelete = activityOccurrenceRepository.findAll().size();

        // Delete the activityOccurrence
        restActivityOccurrenceMockMvc
            .perform(delete("/api/activity-occurrences/{id}", activityOccurrence.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ActivityOccurrence> activityOccurrenceList = activityOccurrenceRepository.findAll();
        assertThat(activityOccurrenceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
