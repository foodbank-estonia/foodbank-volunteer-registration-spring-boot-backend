package ee.foodbank.volunteerregistration.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ee.foodbank.volunteerregistration.IntegrationTest;
import ee.foodbank.volunteerregistration.domain.Volunteer;
import ee.foodbank.volunteerregistration.repository.VolunteerRepository;
import ee.foodbank.volunteerregistration.service.dto.VolunteerDTO;
import ee.foodbank.volunteerregistration.service.mapper.VolunteerMapper;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link VolunteerResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class VolunteerResourceIT {

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_AGE = 5;
    private static final Integer UPDATED_AGE = 6;

    private static final String DEFAULT_PHONE = "+2089557";
    private static final String UPDATED_PHONE = "+13959";

    private static final String DEFAULT_EMAIL = ")!K@-./Pdi=";
    private static final String UPDATED_EMAIL = "K@\"FU\"X.B_-";

    private static final Boolean DEFAULT_IS_GROUP = false;
    private static final Boolean UPDATED_IS_GROUP = true;

    private static final String DEFAULT_GROUP_NAME = "AAAAAAAAAA";
    private static final String UPDATED_GROUP_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_GROUP_PARTICIPANT_COUNT = 2;
    private static final Integer UPDATED_GROUP_PARTICIPANT_COUNT = 3;

    private static final String DEFAULT_NOTES = "AAAAAAAAAA";
    private static final String UPDATED_NOTES = "BBBBBBBBBB";

    @Autowired
    private VolunteerRepository volunteerRepository;

    @Autowired
    private VolunteerMapper volunteerMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restVolunteerMockMvc;

    private Volunteer volunteer;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Volunteer createEntity(EntityManager em) {
        Volunteer volunteer = new Volunteer()
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .age(DEFAULT_AGE)
            .phone(DEFAULT_PHONE)
            .email(DEFAULT_EMAIL)
            .isGroup(DEFAULT_IS_GROUP)
            .groupName(DEFAULT_GROUP_NAME)
            .groupParticipantCount(DEFAULT_GROUP_PARTICIPANT_COUNT)
            .notes(DEFAULT_NOTES);
        return volunteer;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Volunteer createUpdatedEntity(EntityManager em) {
        Volunteer volunteer = new Volunteer()
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .age(UPDATED_AGE)
            .phone(UPDATED_PHONE)
            .email(UPDATED_EMAIL)
            .isGroup(UPDATED_IS_GROUP)
            .groupName(UPDATED_GROUP_NAME)
            .groupParticipantCount(UPDATED_GROUP_PARTICIPANT_COUNT)
            .notes(UPDATED_NOTES);
        return volunteer;
    }

    @BeforeEach
    public void initTest() {
        volunteer = createEntity(em);
    }

    @Test
    @Transactional
    void createVolunteer() throws Exception {
        int databaseSizeBeforeCreate = volunteerRepository.findAll().size();
        // Create the Volunteer
        VolunteerDTO volunteerDTO = volunteerMapper.toDto(volunteer);
        restVolunteerMockMvc
            .perform(
                post("/api/volunteers")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(volunteerDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Volunteer in the database
        List<Volunteer> volunteerList = volunteerRepository.findAll();
        assertThat(volunteerList).hasSize(databaseSizeBeforeCreate + 1);
        Volunteer testVolunteer = volunteerList.get(volunteerList.size() - 1);
        assertThat(testVolunteer.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testVolunteer.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testVolunteer.getAge()).isEqualTo(DEFAULT_AGE);
        assertThat(testVolunteer.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testVolunteer.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testVolunteer.getIsGroup()).isEqualTo(DEFAULT_IS_GROUP);
        assertThat(testVolunteer.getGroupName()).isEqualTo(DEFAULT_GROUP_NAME);
        assertThat(testVolunteer.getGroupParticipantCount()).isEqualTo(DEFAULT_GROUP_PARTICIPANT_COUNT);
        assertThat(testVolunteer.getNotes()).isEqualTo(DEFAULT_NOTES);
    }

    @Test
    @Transactional
    void createVolunteerWithExistingId() throws Exception {
        // Create the Volunteer with an existing ID
        volunteer.setId(1L);
        VolunteerDTO volunteerDTO = volunteerMapper.toDto(volunteer);

        int databaseSizeBeforeCreate = volunteerRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restVolunteerMockMvc
            .perform(
                post("/api/volunteers")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(volunteerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Volunteer in the database
        List<Volunteer> volunteerList = volunteerRepository.findAll();
        assertThat(volunteerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkFirstNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = volunteerRepository.findAll().size();
        // set the field null
        volunteer.setFirstName(null);

        // Create the Volunteer, which fails.
        VolunteerDTO volunteerDTO = volunteerMapper.toDto(volunteer);

        restVolunteerMockMvc
            .perform(
                post("/api/volunteers")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(volunteerDTO))
            )
            .andExpect(status().isBadRequest());

        List<Volunteer> volunteerList = volunteerRepository.findAll();
        assertThat(volunteerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkLastNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = volunteerRepository.findAll().size();
        // set the field null
        volunteer.setLastName(null);

        // Create the Volunteer, which fails.
        VolunteerDTO volunteerDTO = volunteerMapper.toDto(volunteer);

        restVolunteerMockMvc
            .perform(
                post("/api/volunteers")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(volunteerDTO))
            )
            .andExpect(status().isBadRequest());

        List<Volunteer> volunteerList = volunteerRepository.findAll();
        assertThat(volunteerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAgeIsRequired() throws Exception {
        int databaseSizeBeforeTest = volunteerRepository.findAll().size();
        // set the field null
        volunteer.setAge(null);

        // Create the Volunteer, which fails.
        VolunteerDTO volunteerDTO = volunteerMapper.toDto(volunteer);

        restVolunteerMockMvc
            .perform(
                post("/api/volunteers")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(volunteerDTO))
            )
            .andExpect(status().isBadRequest());

        List<Volunteer> volunteerList = volunteerRepository.findAll();
        assertThat(volunteerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPhoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = volunteerRepository.findAll().size();
        // set the field null
        volunteer.setPhone(null);

        // Create the Volunteer, which fails.
        VolunteerDTO volunteerDTO = volunteerMapper.toDto(volunteer);

        restVolunteerMockMvc
            .perform(
                post("/api/volunteers")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(volunteerDTO))
            )
            .andExpect(status().isBadRequest());

        List<Volunteer> volunteerList = volunteerRepository.findAll();
        assertThat(volunteerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = volunteerRepository.findAll().size();
        // set the field null
        volunteer.setEmail(null);

        // Create the Volunteer, which fails.
        VolunteerDTO volunteerDTO = volunteerMapper.toDto(volunteer);

        restVolunteerMockMvc
            .perform(
                post("/api/volunteers")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(volunteerDTO))
            )
            .andExpect(status().isBadRequest());

        List<Volunteer> volunteerList = volunteerRepository.findAll();
        assertThat(volunteerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkIsGroupIsRequired() throws Exception {
        int databaseSizeBeforeTest = volunteerRepository.findAll().size();
        // set the field null
        volunteer.setIsGroup(null);

        // Create the Volunteer, which fails.
        VolunteerDTO volunteerDTO = volunteerMapper.toDto(volunteer);

        restVolunteerMockMvc
            .perform(
                post("/api/volunteers")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(volunteerDTO))
            )
            .andExpect(status().isBadRequest());

        List<Volunteer> volunteerList = volunteerRepository.findAll();
        assertThat(volunteerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllVolunteers() throws Exception {
        // Initialize the database
        volunteerRepository.saveAndFlush(volunteer);

        // Get all the volunteerList
        restVolunteerMockMvc
            .perform(get("/api/volunteers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(volunteer.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].isGroup").value(hasItem(DEFAULT_IS_GROUP.booleanValue())))
            .andExpect(jsonPath("$.[*].groupName").value(hasItem(DEFAULT_GROUP_NAME)))
            .andExpect(jsonPath("$.[*].groupParticipantCount").value(hasItem(DEFAULT_GROUP_PARTICIPANT_COUNT)))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES.toString())));
    }

    @Test
    @Transactional
    void getVolunteer() throws Exception {
        // Initialize the database
        volunteerRepository.saveAndFlush(volunteer);

        // Get the volunteer
        restVolunteerMockMvc
            .perform(get("/api/volunteers/{id}", volunteer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(volunteer.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.age").value(DEFAULT_AGE))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.isGroup").value(DEFAULT_IS_GROUP.booleanValue()))
            .andExpect(jsonPath("$.groupName").value(DEFAULT_GROUP_NAME))
            .andExpect(jsonPath("$.groupParticipantCount").value(DEFAULT_GROUP_PARTICIPANT_COUNT))
            .andExpect(jsonPath("$.notes").value(DEFAULT_NOTES.toString()));
    }

    @Test
    @Transactional
    void getNonExistingVolunteer() throws Exception {
        // Get the volunteer
        restVolunteerMockMvc.perform(get("/api/volunteers/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void updateVolunteer() throws Exception {
        // Initialize the database
        volunteerRepository.saveAndFlush(volunteer);

        int databaseSizeBeforeUpdate = volunteerRepository.findAll().size();

        // Update the volunteer
        Volunteer updatedVolunteer = volunteerRepository.findById(volunteer.getId()).get();
        // Disconnect from session so that the updates on updatedVolunteer are not directly saved in db
        em.detach(updatedVolunteer);
        updatedVolunteer
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .age(UPDATED_AGE)
            .phone(UPDATED_PHONE)
            .email(UPDATED_EMAIL)
            .isGroup(UPDATED_IS_GROUP)
            .groupName(UPDATED_GROUP_NAME)
            .groupParticipantCount(UPDATED_GROUP_PARTICIPANT_COUNT)
            .notes(UPDATED_NOTES);
        VolunteerDTO volunteerDTO = volunteerMapper.toDto(updatedVolunteer);

        restVolunteerMockMvc
            .perform(
                put("/api/volunteers")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(volunteerDTO))
            )
            .andExpect(status().isOk());

        // Validate the Volunteer in the database
        List<Volunteer> volunteerList = volunteerRepository.findAll();
        assertThat(volunteerList).hasSize(databaseSizeBeforeUpdate);
        Volunteer testVolunteer = volunteerList.get(volunteerList.size() - 1);
        assertThat(testVolunteer.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testVolunteer.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testVolunteer.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testVolunteer.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testVolunteer.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testVolunteer.getIsGroup()).isEqualTo(UPDATED_IS_GROUP);
        assertThat(testVolunteer.getGroupName()).isEqualTo(UPDATED_GROUP_NAME);
        assertThat(testVolunteer.getGroupParticipantCount()).isEqualTo(UPDATED_GROUP_PARTICIPANT_COUNT);
        assertThat(testVolunteer.getNotes()).isEqualTo(UPDATED_NOTES);
    }

    @Test
    @Transactional
    void updateNonExistingVolunteer() throws Exception {
        int databaseSizeBeforeUpdate = volunteerRepository.findAll().size();

        // Create the Volunteer
        VolunteerDTO volunteerDTO = volunteerMapper.toDto(volunteer);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVolunteerMockMvc
            .perform(
                put("/api/volunteers")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(volunteerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Volunteer in the database
        List<Volunteer> volunteerList = volunteerRepository.findAll();
        assertThat(volunteerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateVolunteerWithPatch() throws Exception {
        // Initialize the database
        volunteerRepository.saveAndFlush(volunteer);

        int databaseSizeBeforeUpdate = volunteerRepository.findAll().size();

        // Update the volunteer using partial update
        Volunteer partialUpdatedVolunteer = new Volunteer();
        partialUpdatedVolunteer.setId(volunteer.getId());

        partialUpdatedVolunteer
            .age(UPDATED_AGE)
            .phone(UPDATED_PHONE)
            .email(UPDATED_EMAIL)
            .isGroup(UPDATED_IS_GROUP)
            .groupParticipantCount(UPDATED_GROUP_PARTICIPANT_COUNT);

        restVolunteerMockMvc
            .perform(
                patch("/api/volunteers")
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedVolunteer))
            )
            .andExpect(status().isOk());

        // Validate the Volunteer in the database
        List<Volunteer> volunteerList = volunteerRepository.findAll();
        assertThat(volunteerList).hasSize(databaseSizeBeforeUpdate);
        Volunteer testVolunteer = volunteerList.get(volunteerList.size() - 1);
        assertThat(testVolunteer.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testVolunteer.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testVolunteer.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testVolunteer.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testVolunteer.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testVolunteer.getIsGroup()).isEqualTo(UPDATED_IS_GROUP);
        assertThat(testVolunteer.getGroupName()).isEqualTo(DEFAULT_GROUP_NAME);
        assertThat(testVolunteer.getGroupParticipantCount()).isEqualTo(UPDATED_GROUP_PARTICIPANT_COUNT);
        assertThat(testVolunteer.getNotes()).isEqualTo(DEFAULT_NOTES);
    }

    @Test
    @Transactional
    void fullUpdateVolunteerWithPatch() throws Exception {
        // Initialize the database
        volunteerRepository.saveAndFlush(volunteer);

        int databaseSizeBeforeUpdate = volunteerRepository.findAll().size();

        // Update the volunteer using partial update
        Volunteer partialUpdatedVolunteer = new Volunteer();
        partialUpdatedVolunteer.setId(volunteer.getId());

        partialUpdatedVolunteer
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .age(UPDATED_AGE)
            .phone(UPDATED_PHONE)
            .email(UPDATED_EMAIL)
            .isGroup(UPDATED_IS_GROUP)
            .groupName(UPDATED_GROUP_NAME)
            .groupParticipantCount(UPDATED_GROUP_PARTICIPANT_COUNT)
            .notes(UPDATED_NOTES);

        restVolunteerMockMvc
            .perform(
                patch("/api/volunteers")
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedVolunteer))
            )
            .andExpect(status().isOk());

        // Validate the Volunteer in the database
        List<Volunteer> volunteerList = volunteerRepository.findAll();
        assertThat(volunteerList).hasSize(databaseSizeBeforeUpdate);
        Volunteer testVolunteer = volunteerList.get(volunteerList.size() - 1);
        assertThat(testVolunteer.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testVolunteer.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testVolunteer.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testVolunteer.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testVolunteer.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testVolunteer.getIsGroup()).isEqualTo(UPDATED_IS_GROUP);
        assertThat(testVolunteer.getGroupName()).isEqualTo(UPDATED_GROUP_NAME);
        assertThat(testVolunteer.getGroupParticipantCount()).isEqualTo(UPDATED_GROUP_PARTICIPANT_COUNT);
        assertThat(testVolunteer.getNotes()).isEqualTo(UPDATED_NOTES);
    }

    @Test
    @Transactional
    void partialUpdateVolunteerShouldThrown() throws Exception {
        // Update the volunteer without id should throw
        Volunteer partialUpdatedVolunteer = new Volunteer();

        restVolunteerMockMvc
            .perform(
                patch("/api/volunteers")
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedVolunteer))
            )
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    void deleteVolunteer() throws Exception {
        // Initialize the database
        volunteerRepository.saveAndFlush(volunteer);

        int databaseSizeBeforeDelete = volunteerRepository.findAll().size();

        // Delete the volunteer
        restVolunteerMockMvc
            .perform(delete("/api/volunteers/{id}", volunteer.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Volunteer> volunteerList = volunteerRepository.findAll();
        assertThat(volunteerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
