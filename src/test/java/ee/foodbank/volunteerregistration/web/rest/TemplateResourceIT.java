package ee.foodbank.volunteerregistration.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ee.foodbank.volunteerregistration.IntegrationTest;
import ee.foodbank.volunteerregistration.domain.Template;
import ee.foodbank.volunteerregistration.repository.TemplateRepository;
import ee.foodbank.volunteerregistration.service.dto.TemplateDTO;
import ee.foodbank.volunteerregistration.service.mapper.TemplateMapper;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link TemplateResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class TemplateResourceIT {

    private static final String DEFAULT_PAGE_HEADER = "AAAAAAAAAA";
    private static final String UPDATED_PAGE_HEADER = "BBBBBBBBBB";

    private static final String DEFAULT_PAGE_FOOTER = "AAAAAAAAAA";
    private static final String UPDATED_PAGE_FOOTER = "BBBBBBBBBB";

    private static final String DEFAULT_QUALITY_BADGE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_QUALITY_BADGE_TEXT = "BBBBBBBBBB";

    @Autowired
    private TemplateRepository templateRepository;

    @Autowired
    private TemplateMapper templateMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTemplateMockMvc;

    private Template template;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Template createEntity(EntityManager em) {
        Template template = new Template()
            .pageHeader(DEFAULT_PAGE_HEADER)
            .pageFooter(DEFAULT_PAGE_FOOTER)
            .qualityBadgeText(DEFAULT_QUALITY_BADGE_TEXT);
        return template;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Template createUpdatedEntity(EntityManager em) {
        Template template = new Template()
            .pageHeader(UPDATED_PAGE_HEADER)
            .pageFooter(UPDATED_PAGE_FOOTER)
            .qualityBadgeText(UPDATED_QUALITY_BADGE_TEXT);
        return template;
    }

    @BeforeEach
    public void initTest() {
        template = createEntity(em);
    }

    @Test
    @Transactional
    void createTemplate() throws Exception {
        int databaseSizeBeforeCreate = templateRepository.findAll().size();
        // Create the Template
        TemplateDTO templateDTO = templateMapper.toDto(template);
        restTemplateMockMvc
            .perform(
                post("/api/templates")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(templateDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Template in the database
        List<Template> templateList = templateRepository.findAll();
        assertThat(templateList).hasSize(databaseSizeBeforeCreate + 1);
        Template testTemplate = templateList.get(templateList.size() - 1);
        assertThat(testTemplate.getPageHeader()).isEqualTo(DEFAULT_PAGE_HEADER);
        assertThat(testTemplate.getPageFooter()).isEqualTo(DEFAULT_PAGE_FOOTER);
        assertThat(testTemplate.getQualityBadgeText()).isEqualTo(DEFAULT_QUALITY_BADGE_TEXT);
    }

    @Test
    @Transactional
    void createTemplateWithExistingId() throws Exception {
        // Create the Template with an existing ID
        template.setId(1L);
        TemplateDTO templateDTO = templateMapper.toDto(template);

        int databaseSizeBeforeCreate = templateRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restTemplateMockMvc
            .perform(
                post("/api/templates")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(templateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Template in the database
        List<Template> templateList = templateRepository.findAll();
        assertThat(templateList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllTemplates() throws Exception {
        // Initialize the database
        templateRepository.saveAndFlush(template);

        // Get all the templateList
        restTemplateMockMvc
            .perform(get("/api/templates?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(template.getId().intValue())))
            .andExpect(jsonPath("$.[*].pageHeader").value(hasItem(DEFAULT_PAGE_HEADER.toString())))
            .andExpect(jsonPath("$.[*].pageFooter").value(hasItem(DEFAULT_PAGE_FOOTER.toString())))
            .andExpect(jsonPath("$.[*].qualityBadgeText").value(hasItem(DEFAULT_QUALITY_BADGE_TEXT.toString())));
    }

    @Test
    @Transactional
    void getTemplate() throws Exception {
        // Initialize the database
        templateRepository.saveAndFlush(template);

        // Get the template
        restTemplateMockMvc
            .perform(get("/api/templates/{id}", template.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(template.getId().intValue()))
            .andExpect(jsonPath("$.pageHeader").value(DEFAULT_PAGE_HEADER.toString()))
            .andExpect(jsonPath("$.pageFooter").value(DEFAULT_PAGE_FOOTER.toString()))
            .andExpect(jsonPath("$.qualityBadgeText").value(DEFAULT_QUALITY_BADGE_TEXT.toString()));
    }

    @Test
    @Transactional
    void getNonExistingTemplate() throws Exception {
        // Get the template
        restTemplateMockMvc.perform(get("/api/templates/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void updateTemplate() throws Exception {
        // Initialize the database
        templateRepository.saveAndFlush(template);

        int databaseSizeBeforeUpdate = templateRepository.findAll().size();

        // Update the template
        Template updatedTemplate = templateRepository.findById(template.getId()).get();
        // Disconnect from session so that the updates on updatedTemplate are not directly saved in db
        em.detach(updatedTemplate);
        updatedTemplate.pageHeader(UPDATED_PAGE_HEADER).pageFooter(UPDATED_PAGE_FOOTER).qualityBadgeText(UPDATED_QUALITY_BADGE_TEXT);
        TemplateDTO templateDTO = templateMapper.toDto(updatedTemplate);

        restTemplateMockMvc
            .perform(
                put("/api/templates")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(templateDTO))
            )
            .andExpect(status().isOk());

        // Validate the Template in the database
        List<Template> templateList = templateRepository.findAll();
        assertThat(templateList).hasSize(databaseSizeBeforeUpdate);
        Template testTemplate = templateList.get(templateList.size() - 1);
        assertThat(testTemplate.getPageHeader()).isEqualTo(UPDATED_PAGE_HEADER);
        assertThat(testTemplate.getPageFooter()).isEqualTo(UPDATED_PAGE_FOOTER);
        assertThat(testTemplate.getQualityBadgeText()).isEqualTo(UPDATED_QUALITY_BADGE_TEXT);
    }

    @Test
    @Transactional
    void updateNonExistingTemplate() throws Exception {
        int databaseSizeBeforeUpdate = templateRepository.findAll().size();

        // Create the Template
        TemplateDTO templateDTO = templateMapper.toDto(template);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTemplateMockMvc
            .perform(
                put("/api/templates")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(templateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Template in the database
        List<Template> templateList = templateRepository.findAll();
        assertThat(templateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateTemplateWithPatch() throws Exception {
        // Initialize the database
        templateRepository.saveAndFlush(template);

        int databaseSizeBeforeUpdate = templateRepository.findAll().size();

        // Update the template using partial update
        Template partialUpdatedTemplate = new Template();
        partialUpdatedTemplate.setId(template.getId());

        partialUpdatedTemplate.pageHeader(UPDATED_PAGE_HEADER).pageFooter(UPDATED_PAGE_FOOTER).qualityBadgeText(UPDATED_QUALITY_BADGE_TEXT);

        restTemplateMockMvc
            .perform(
                patch("/api/templates")
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTemplate))
            )
            .andExpect(status().isOk());

        // Validate the Template in the database
        List<Template> templateList = templateRepository.findAll();
        assertThat(templateList).hasSize(databaseSizeBeforeUpdate);
        Template testTemplate = templateList.get(templateList.size() - 1);
        assertThat(testTemplate.getPageHeader()).isEqualTo(UPDATED_PAGE_HEADER);
        assertThat(testTemplate.getPageFooter()).isEqualTo(UPDATED_PAGE_FOOTER);
        assertThat(testTemplate.getQualityBadgeText()).isEqualTo(UPDATED_QUALITY_BADGE_TEXT);
    }

    @Test
    @Transactional
    void fullUpdateTemplateWithPatch() throws Exception {
        // Initialize the database
        templateRepository.saveAndFlush(template);

        int databaseSizeBeforeUpdate = templateRepository.findAll().size();

        // Update the template using partial update
        Template partialUpdatedTemplate = new Template();
        partialUpdatedTemplate.setId(template.getId());

        partialUpdatedTemplate.pageHeader(UPDATED_PAGE_HEADER).pageFooter(UPDATED_PAGE_FOOTER).qualityBadgeText(UPDATED_QUALITY_BADGE_TEXT);

        restTemplateMockMvc
            .perform(
                patch("/api/templates")
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTemplate))
            )
            .andExpect(status().isOk());

        // Validate the Template in the database
        List<Template> templateList = templateRepository.findAll();
        assertThat(templateList).hasSize(databaseSizeBeforeUpdate);
        Template testTemplate = templateList.get(templateList.size() - 1);
        assertThat(testTemplate.getPageHeader()).isEqualTo(UPDATED_PAGE_HEADER);
        assertThat(testTemplate.getPageFooter()).isEqualTo(UPDATED_PAGE_FOOTER);
        assertThat(testTemplate.getQualityBadgeText()).isEqualTo(UPDATED_QUALITY_BADGE_TEXT);
    }

    @Test
    @Transactional
    void partialUpdateTemplateShouldThrown() throws Exception {
        // Update the template without id should throw
        Template partialUpdatedTemplate = new Template();

        restTemplateMockMvc
            .perform(
                patch("/api/templates")
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTemplate))
            )
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    void deleteTemplate() throws Exception {
        // Initialize the database
        templateRepository.saveAndFlush(template);

        int databaseSizeBeforeDelete = templateRepository.findAll().size();

        // Delete the template
        restTemplateMockMvc
            .perform(delete("/api/templates/{id}", template.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Template> templateList = templateRepository.findAll();
        assertThat(templateList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
