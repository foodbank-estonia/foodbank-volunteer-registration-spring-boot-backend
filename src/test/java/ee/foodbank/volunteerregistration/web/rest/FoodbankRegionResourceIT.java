package ee.foodbank.volunteerregistration.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ee.foodbank.volunteerregistration.IntegrationTest;
import ee.foodbank.volunteerregistration.domain.FoodbankRegion;
import ee.foodbank.volunteerregistration.repository.FoodbankRegionRepository;
import ee.foodbank.volunteerregistration.service.dto.FoodbankRegionDTO;
import ee.foodbank.volunteerregistration.service.mapper.FoodbankRegionMapper;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link FoodbankRegionResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class FoodbankRegionResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Boolean DEFAULT_QUALITY_BADGE = false;
    private static final Boolean UPDATED_QUALITY_BADGE = true;

    @Autowired
    private FoodbankRegionRepository foodbankRegionRepository;

    @Autowired
    private FoodbankRegionMapper foodbankRegionMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFoodbankRegionMockMvc;

    private FoodbankRegion foodbankRegion;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FoodbankRegion createEntity(EntityManager em) {
        FoodbankRegion foodbankRegion = new FoodbankRegion()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .qualityBadge(DEFAULT_QUALITY_BADGE);
        return foodbankRegion;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FoodbankRegion createUpdatedEntity(EntityManager em) {
        FoodbankRegion foodbankRegion = new FoodbankRegion()
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .qualityBadge(UPDATED_QUALITY_BADGE);
        return foodbankRegion;
    }

    @BeforeEach
    public void initTest() {
        foodbankRegion = createEntity(em);
    }

    @Test
    @Transactional
    void createFoodbankRegion() throws Exception {
        int databaseSizeBeforeCreate = foodbankRegionRepository.findAll().size();
        // Create the FoodbankRegion
        FoodbankRegionDTO foodbankRegionDTO = foodbankRegionMapper.toDto(foodbankRegion);
        restFoodbankRegionMockMvc
            .perform(
                post("/api/foodbank-regions")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(foodbankRegionDTO))
            )
            .andExpect(status().isCreated());

        // Validate the FoodbankRegion in the database
        List<FoodbankRegion> foodbankRegionList = foodbankRegionRepository.findAll();
        assertThat(foodbankRegionList).hasSize(databaseSizeBeforeCreate + 1);
        FoodbankRegion testFoodbankRegion = foodbankRegionList.get(foodbankRegionList.size() - 1);
        assertThat(testFoodbankRegion.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testFoodbankRegion.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testFoodbankRegion.getQualityBadge()).isEqualTo(DEFAULT_QUALITY_BADGE);
    }

    @Test
    @Transactional
    void createFoodbankRegionWithExistingId() throws Exception {
        // Create the FoodbankRegion with an existing ID
        foodbankRegion.setId(1L);
        FoodbankRegionDTO foodbankRegionDTO = foodbankRegionMapper.toDto(foodbankRegion);

        int databaseSizeBeforeCreate = foodbankRegionRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restFoodbankRegionMockMvc
            .perform(
                post("/api/foodbank-regions")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(foodbankRegionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the FoodbankRegion in the database
        List<FoodbankRegion> foodbankRegionList = foodbankRegionRepository.findAll();
        assertThat(foodbankRegionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = foodbankRegionRepository.findAll().size();
        // set the field null
        foodbankRegion.setName(null);

        // Create the FoodbankRegion, which fails.
        FoodbankRegionDTO foodbankRegionDTO = foodbankRegionMapper.toDto(foodbankRegion);

        restFoodbankRegionMockMvc
            .perform(
                post("/api/foodbank-regions")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(foodbankRegionDTO))
            )
            .andExpect(status().isBadRequest());

        List<FoodbankRegion> foodbankRegionList = foodbankRegionRepository.findAll();
        assertThat(foodbankRegionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllFoodbankRegions() throws Exception {
        // Initialize the database
        foodbankRegionRepository.saveAndFlush(foodbankRegion);

        // Get all the foodbankRegionList
        restFoodbankRegionMockMvc
            .perform(get("/api/foodbank-regions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(foodbankRegion.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].qualityBadge").value(hasItem(DEFAULT_QUALITY_BADGE.booleanValue())));
    }

    @Test
    @Transactional
    void getFoodbankRegion() throws Exception {
        // Initialize the database
        foodbankRegionRepository.saveAndFlush(foodbankRegion);

        // Get the foodbankRegion
        restFoodbankRegionMockMvc
            .perform(get("/api/foodbank-regions/{id}", foodbankRegion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(foodbankRegion.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.qualityBadge").value(DEFAULT_QUALITY_BADGE.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingFoodbankRegion() throws Exception {
        // Get the foodbankRegion
        restFoodbankRegionMockMvc.perform(get("/api/foodbank-regions/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void updateFoodbankRegion() throws Exception {
        // Initialize the database
        foodbankRegionRepository.saveAndFlush(foodbankRegion);

        int databaseSizeBeforeUpdate = foodbankRegionRepository.findAll().size();

        // Update the foodbankRegion
        FoodbankRegion updatedFoodbankRegion = foodbankRegionRepository.findById(foodbankRegion.getId()).get();
        // Disconnect from session so that the updates on updatedFoodbankRegion are not directly saved in db
        em.detach(updatedFoodbankRegion);
        updatedFoodbankRegion.name(UPDATED_NAME).description(UPDATED_DESCRIPTION).qualityBadge(UPDATED_QUALITY_BADGE);
        FoodbankRegionDTO foodbankRegionDTO = foodbankRegionMapper.toDto(updatedFoodbankRegion);

        restFoodbankRegionMockMvc
            .perform(
                put("/api/foodbank-regions")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(foodbankRegionDTO))
            )
            .andExpect(status().isOk());

        // Validate the FoodbankRegion in the database
        List<FoodbankRegion> foodbankRegionList = foodbankRegionRepository.findAll();
        assertThat(foodbankRegionList).hasSize(databaseSizeBeforeUpdate);
        FoodbankRegion testFoodbankRegion = foodbankRegionList.get(foodbankRegionList.size() - 1);
        assertThat(testFoodbankRegion.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testFoodbankRegion.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testFoodbankRegion.getQualityBadge()).isEqualTo(UPDATED_QUALITY_BADGE);
    }

    @Test
    @Transactional
    void updateNonExistingFoodbankRegion() throws Exception {
        int databaseSizeBeforeUpdate = foodbankRegionRepository.findAll().size();

        // Create the FoodbankRegion
        FoodbankRegionDTO foodbankRegionDTO = foodbankRegionMapper.toDto(foodbankRegion);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFoodbankRegionMockMvc
            .perform(
                put("/api/foodbank-regions")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(foodbankRegionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the FoodbankRegion in the database
        List<FoodbankRegion> foodbankRegionList = foodbankRegionRepository.findAll();
        assertThat(foodbankRegionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateFoodbankRegionWithPatch() throws Exception {
        // Initialize the database
        foodbankRegionRepository.saveAndFlush(foodbankRegion);

        int databaseSizeBeforeUpdate = foodbankRegionRepository.findAll().size();

        // Update the foodbankRegion using partial update
        FoodbankRegion partialUpdatedFoodbankRegion = new FoodbankRegion();
        partialUpdatedFoodbankRegion.setId(foodbankRegion.getId());

        partialUpdatedFoodbankRegion.name(UPDATED_NAME);

        restFoodbankRegionMockMvc
            .perform(
                patch("/api/foodbank-regions")
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFoodbankRegion))
            )
            .andExpect(status().isOk());

        // Validate the FoodbankRegion in the database
        List<FoodbankRegion> foodbankRegionList = foodbankRegionRepository.findAll();
        assertThat(foodbankRegionList).hasSize(databaseSizeBeforeUpdate);
        FoodbankRegion testFoodbankRegion = foodbankRegionList.get(foodbankRegionList.size() - 1);
        assertThat(testFoodbankRegion.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testFoodbankRegion.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testFoodbankRegion.getQualityBadge()).isEqualTo(DEFAULT_QUALITY_BADGE);
    }

    @Test
    @Transactional
    void fullUpdateFoodbankRegionWithPatch() throws Exception {
        // Initialize the database
        foodbankRegionRepository.saveAndFlush(foodbankRegion);

        int databaseSizeBeforeUpdate = foodbankRegionRepository.findAll().size();

        // Update the foodbankRegion using partial update
        FoodbankRegion partialUpdatedFoodbankRegion = new FoodbankRegion();
        partialUpdatedFoodbankRegion.setId(foodbankRegion.getId());

        partialUpdatedFoodbankRegion.name(UPDATED_NAME).description(UPDATED_DESCRIPTION).qualityBadge(UPDATED_QUALITY_BADGE);

        restFoodbankRegionMockMvc
            .perform(
                patch("/api/foodbank-regions")
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFoodbankRegion))
            )
            .andExpect(status().isOk());

        // Validate the FoodbankRegion in the database
        List<FoodbankRegion> foodbankRegionList = foodbankRegionRepository.findAll();
        assertThat(foodbankRegionList).hasSize(databaseSizeBeforeUpdate);
        FoodbankRegion testFoodbankRegion = foodbankRegionList.get(foodbankRegionList.size() - 1);
        assertThat(testFoodbankRegion.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testFoodbankRegion.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testFoodbankRegion.getQualityBadge()).isEqualTo(UPDATED_QUALITY_BADGE);
    }

    @Test
    @Transactional
    void partialUpdateFoodbankRegionShouldThrown() throws Exception {
        // Update the foodbankRegion without id should throw
        FoodbankRegion partialUpdatedFoodbankRegion = new FoodbankRegion();

        restFoodbankRegionMockMvc
            .perform(
                patch("/api/foodbank-regions")
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFoodbankRegion))
            )
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    void deleteFoodbankRegion() throws Exception {
        // Initialize the database
        foodbankRegionRepository.saveAndFlush(foodbankRegion);

        int databaseSizeBeforeDelete = foodbankRegionRepository.findAll().size();

        // Delete the foodbankRegion
        restFoodbankRegionMockMvc
            .perform(delete("/api/foodbank-regions/{id}", foodbankRegion.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FoodbankRegion> foodbankRegionList = foodbankRegionRepository.findAll();
        assertThat(foodbankRegionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
