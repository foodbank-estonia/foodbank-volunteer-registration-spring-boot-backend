package ee.foodbank.volunteerregistration.web.rest;

import static ee.foodbank.volunteerregistration.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ee.foodbank.volunteerregistration.IntegrationTest;
import ee.foodbank.volunteerregistration.domain.ActivityOccurrence;
import ee.foodbank.volunteerregistration.domain.Volunteer;
import ee.foodbank.volunteerregistration.domain.VolunteerRegistration;
import ee.foodbank.volunteerregistration.repository.VolunteerRegistrationRepository;
import ee.foodbank.volunteerregistration.service.dto.VolunteerRegistrationDTO;
import ee.foodbank.volunteerregistration.service.mapper.VolunteerRegistrationMapper;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link VolunteerRegistrationResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class VolunteerRegistrationResourceIT {

    private static final ZonedDateTime DEFAULT_REGISTRATION_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_REGISTRATION_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private VolunteerRegistrationRepository volunteerRegistrationRepository;

    @Autowired
    private VolunteerRegistrationMapper volunteerRegistrationMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restVolunteerRegistrationMockMvc;

    private VolunteerRegistration volunteerRegistration;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VolunteerRegistration createEntity(EntityManager em) {
        VolunteerRegistration volunteerRegistration = new VolunteerRegistration().registrationTime(DEFAULT_REGISTRATION_TIME);
        // Add required entity
        ActivityOccurrence activityOccurrence;
        if (TestUtil.findAll(em, ActivityOccurrence.class).isEmpty()) {
            activityOccurrence = ActivityOccurrenceResourceIT.createEntity(em);
            em.persist(activityOccurrence);
            em.flush();
        } else {
            activityOccurrence = TestUtil.findAll(em, ActivityOccurrence.class).get(0);
        }
        volunteerRegistration.setActivityOccurrence(activityOccurrence);
        // Add required entity
        Volunteer volunteer;
        if (TestUtil.findAll(em, Volunteer.class).isEmpty()) {
            volunteer = VolunteerResourceIT.createEntity(em);
            em.persist(volunteer);
            em.flush();
        } else {
            volunteer = TestUtil.findAll(em, Volunteer.class).get(0);
        }
        volunteerRegistration.setVolunteer(volunteer);
        return volunteerRegistration;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VolunteerRegistration createUpdatedEntity(EntityManager em) {
        VolunteerRegistration volunteerRegistration = new VolunteerRegistration().registrationTime(UPDATED_REGISTRATION_TIME);
        // Add required entity
        ActivityOccurrence activityOccurrence;
        if (TestUtil.findAll(em, ActivityOccurrence.class).isEmpty()) {
            activityOccurrence = ActivityOccurrenceResourceIT.createUpdatedEntity(em);
            em.persist(activityOccurrence);
            em.flush();
        } else {
            activityOccurrence = TestUtil.findAll(em, ActivityOccurrence.class).get(0);
        }
        volunteerRegistration.setActivityOccurrence(activityOccurrence);
        // Add required entity
        Volunteer volunteer;
        if (TestUtil.findAll(em, Volunteer.class).isEmpty()) {
            volunteer = VolunteerResourceIT.createUpdatedEntity(em);
            em.persist(volunteer);
            em.flush();
        } else {
            volunteer = TestUtil.findAll(em, Volunteer.class).get(0);
        }
        volunteerRegistration.setVolunteer(volunteer);
        return volunteerRegistration;
    }

    @BeforeEach
    public void initTest() {
        volunteerRegistration = createEntity(em);
    }

    @Test
    @Transactional
    void createVolunteerRegistration() throws Exception {
        int databaseSizeBeforeCreate = volunteerRegistrationRepository.findAll().size();
        // Create the VolunteerRegistration
        VolunteerRegistrationDTO volunteerRegistrationDTO = volunteerRegistrationMapper.toDto(volunteerRegistration);
        restVolunteerRegistrationMockMvc
            .perform(
                post("/api/volunteer-registrations")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(volunteerRegistrationDTO))
            )
            .andExpect(status().isCreated());

        // Validate the VolunteerRegistration in the database
        List<VolunteerRegistration> volunteerRegistrationList = volunteerRegistrationRepository.findAll();
        assertThat(volunteerRegistrationList).hasSize(databaseSizeBeforeCreate + 1);
        VolunteerRegistration testVolunteerRegistration = volunteerRegistrationList.get(volunteerRegistrationList.size() - 1);
        assertThat(testVolunteerRegistration.getRegistrationTime()).isEqualTo(DEFAULT_REGISTRATION_TIME);
    }

    @Test
    @Transactional
    void createVolunteerRegistrationWithExistingId() throws Exception {
        // Create the VolunteerRegistration with an existing ID
        volunteerRegistration.setId(1L);
        VolunteerRegistrationDTO volunteerRegistrationDTO = volunteerRegistrationMapper.toDto(volunteerRegistration);

        int databaseSizeBeforeCreate = volunteerRegistrationRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restVolunteerRegistrationMockMvc
            .perform(
                post("/api/volunteer-registrations")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(volunteerRegistrationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the VolunteerRegistration in the database
        List<VolunteerRegistration> volunteerRegistrationList = volunteerRegistrationRepository.findAll();
        assertThat(volunteerRegistrationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkRegistrationTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = volunteerRegistrationRepository.findAll().size();
        // set the field null
        volunteerRegistration.setRegistrationTime(null);

        // Create the VolunteerRegistration, which fails.
        VolunteerRegistrationDTO volunteerRegistrationDTO = volunteerRegistrationMapper.toDto(volunteerRegistration);

        restVolunteerRegistrationMockMvc
            .perform(
                post("/api/volunteer-registrations")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(volunteerRegistrationDTO))
            )
            .andExpect(status().isBadRequest());

        List<VolunteerRegistration> volunteerRegistrationList = volunteerRegistrationRepository.findAll();
        assertThat(volunteerRegistrationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllVolunteerRegistrations() throws Exception {
        // Initialize the database
        volunteerRegistrationRepository.saveAndFlush(volunteerRegistration);

        // Get all the volunteerRegistrationList
        restVolunteerRegistrationMockMvc
            .perform(get("/api/volunteer-registrations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(volunteerRegistration.getId().intValue())))
            .andExpect(jsonPath("$.[*].registrationTime").value(hasItem(sameInstant(DEFAULT_REGISTRATION_TIME))));
    }

    @Test
    @Transactional
    void getVolunteerRegistration() throws Exception {
        // Initialize the database
        volunteerRegistrationRepository.saveAndFlush(volunteerRegistration);

        // Get the volunteerRegistration
        restVolunteerRegistrationMockMvc
            .perform(get("/api/volunteer-registrations/{id}", volunteerRegistration.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(volunteerRegistration.getId().intValue()))
            .andExpect(jsonPath("$.registrationTime").value(sameInstant(DEFAULT_REGISTRATION_TIME)));
    }

    @Test
    @Transactional
    void getNonExistingVolunteerRegistration() throws Exception {
        // Get the volunteerRegistration
        restVolunteerRegistrationMockMvc.perform(get("/api/volunteer-registrations/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void updateVolunteerRegistration() throws Exception {
        // Initialize the database
        volunteerRegistrationRepository.saveAndFlush(volunteerRegistration);

        int databaseSizeBeforeUpdate = volunteerRegistrationRepository.findAll().size();

        // Update the volunteerRegistration
        VolunteerRegistration updatedVolunteerRegistration = volunteerRegistrationRepository.findById(volunteerRegistration.getId()).get();
        // Disconnect from session so that the updates on updatedVolunteerRegistration are not directly saved in db
        em.detach(updatedVolunteerRegistration);
        updatedVolunteerRegistration.registrationTime(UPDATED_REGISTRATION_TIME);
        VolunteerRegistrationDTO volunteerRegistrationDTO = volunteerRegistrationMapper.toDto(updatedVolunteerRegistration);

        restVolunteerRegistrationMockMvc
            .perform(
                put("/api/volunteer-registrations")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(volunteerRegistrationDTO))
            )
            .andExpect(status().isOk());

        // Validate the VolunteerRegistration in the database
        List<VolunteerRegistration> volunteerRegistrationList = volunteerRegistrationRepository.findAll();
        assertThat(volunteerRegistrationList).hasSize(databaseSizeBeforeUpdate);
        VolunteerRegistration testVolunteerRegistration = volunteerRegistrationList.get(volunteerRegistrationList.size() - 1);
        assertThat(testVolunteerRegistration.getRegistrationTime()).isEqualTo(UPDATED_REGISTRATION_TIME);
    }

    @Test
    @Transactional
    void updateNonExistingVolunteerRegistration() throws Exception {
        int databaseSizeBeforeUpdate = volunteerRegistrationRepository.findAll().size();

        // Create the VolunteerRegistration
        VolunteerRegistrationDTO volunteerRegistrationDTO = volunteerRegistrationMapper.toDto(volunteerRegistration);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVolunteerRegistrationMockMvc
            .perform(
                put("/api/volunteer-registrations")
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(volunteerRegistrationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the VolunteerRegistration in the database
        List<VolunteerRegistration> volunteerRegistrationList = volunteerRegistrationRepository.findAll();
        assertThat(volunteerRegistrationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateVolunteerRegistrationWithPatch() throws Exception {
        // Initialize the database
        volunteerRegistrationRepository.saveAndFlush(volunteerRegistration);

        int databaseSizeBeforeUpdate = volunteerRegistrationRepository.findAll().size();

        // Update the volunteerRegistration using partial update
        VolunteerRegistration partialUpdatedVolunteerRegistration = new VolunteerRegistration();
        partialUpdatedVolunteerRegistration.setId(volunteerRegistration.getId());

        restVolunteerRegistrationMockMvc
            .perform(
                patch("/api/volunteer-registrations")
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedVolunteerRegistration))
            )
            .andExpect(status().isOk());

        // Validate the VolunteerRegistration in the database
        List<VolunteerRegistration> volunteerRegistrationList = volunteerRegistrationRepository.findAll();
        assertThat(volunteerRegistrationList).hasSize(databaseSizeBeforeUpdate);
        VolunteerRegistration testVolunteerRegistration = volunteerRegistrationList.get(volunteerRegistrationList.size() - 1);
        assertThat(testVolunteerRegistration.getRegistrationTime()).isEqualTo(DEFAULT_REGISTRATION_TIME);
    }

    @Test
    @Transactional
    void fullUpdateVolunteerRegistrationWithPatch() throws Exception {
        // Initialize the database
        volunteerRegistrationRepository.saveAndFlush(volunteerRegistration);

        int databaseSizeBeforeUpdate = volunteerRegistrationRepository.findAll().size();

        // Update the volunteerRegistration using partial update
        VolunteerRegistration partialUpdatedVolunteerRegistration = new VolunteerRegistration();
        partialUpdatedVolunteerRegistration.setId(volunteerRegistration.getId());

        partialUpdatedVolunteerRegistration.registrationTime(UPDATED_REGISTRATION_TIME);

        restVolunteerRegistrationMockMvc
            .perform(
                patch("/api/volunteer-registrations")
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedVolunteerRegistration))
            )
            .andExpect(status().isOk());

        // Validate the VolunteerRegistration in the database
        List<VolunteerRegistration> volunteerRegistrationList = volunteerRegistrationRepository.findAll();
        assertThat(volunteerRegistrationList).hasSize(databaseSizeBeforeUpdate);
        VolunteerRegistration testVolunteerRegistration = volunteerRegistrationList.get(volunteerRegistrationList.size() - 1);
        assertThat(testVolunteerRegistration.getRegistrationTime()).isEqualTo(UPDATED_REGISTRATION_TIME);
    }

    @Test
    @Transactional
    void partialUpdateVolunteerRegistrationShouldThrown() throws Exception {
        // Update the volunteerRegistration without id should throw
        VolunteerRegistration partialUpdatedVolunteerRegistration = new VolunteerRegistration();

        restVolunteerRegistrationMockMvc
            .perform(
                patch("/api/volunteer-registrations")
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedVolunteerRegistration))
            )
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    void deleteVolunteerRegistration() throws Exception {
        // Initialize the database
        volunteerRegistrationRepository.saveAndFlush(volunteerRegistration);

        int databaseSizeBeforeDelete = volunteerRegistrationRepository.findAll().size();

        // Delete the volunteerRegistration
        restVolunteerRegistrationMockMvc
            .perform(
                delete("/api/volunteer-registrations/{id}", volunteerRegistration.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<VolunteerRegistration> volunteerRegistrationList = volunteerRegistrationRepository.findAll();
        assertThat(volunteerRegistrationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
