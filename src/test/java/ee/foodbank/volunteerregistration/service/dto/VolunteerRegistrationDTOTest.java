package ee.foodbank.volunteerregistration.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import ee.foodbank.volunteerregistration.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class VolunteerRegistrationDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(VolunteerRegistrationDTO.class);
        VolunteerRegistrationDTO volunteerRegistrationDTO1 = new VolunteerRegistrationDTO();
        volunteerRegistrationDTO1.setId(1L);
        VolunteerRegistrationDTO volunteerRegistrationDTO2 = new VolunteerRegistrationDTO();
        assertThat(volunteerRegistrationDTO1).isNotEqualTo(volunteerRegistrationDTO2);
        volunteerRegistrationDTO2.setId(volunteerRegistrationDTO1.getId());
        assertThat(volunteerRegistrationDTO1).isEqualTo(volunteerRegistrationDTO2);
        volunteerRegistrationDTO2.setId(2L);
        assertThat(volunteerRegistrationDTO1).isNotEqualTo(volunteerRegistrationDTO2);
        volunteerRegistrationDTO1.setId(null);
        assertThat(volunteerRegistrationDTO1).isNotEqualTo(volunteerRegistrationDTO2);
    }
}
