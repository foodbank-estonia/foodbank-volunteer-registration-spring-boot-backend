package ee.foodbank.volunteerregistration.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import ee.foodbank.volunteerregistration.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ActivityOccurrenceDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ActivityOccurrenceDTO.class);
        ActivityOccurrenceDTO activityOccurrenceDTO1 = new ActivityOccurrenceDTO();
        activityOccurrenceDTO1.setId(1L);
        ActivityOccurrenceDTO activityOccurrenceDTO2 = new ActivityOccurrenceDTO();
        assertThat(activityOccurrenceDTO1).isNotEqualTo(activityOccurrenceDTO2);
        activityOccurrenceDTO2.setId(activityOccurrenceDTO1.getId());
        assertThat(activityOccurrenceDTO1).isEqualTo(activityOccurrenceDTO2);
        activityOccurrenceDTO2.setId(2L);
        assertThat(activityOccurrenceDTO1).isNotEqualTo(activityOccurrenceDTO2);
        activityOccurrenceDTO1.setId(null);
        assertThat(activityOccurrenceDTO1).isNotEqualTo(activityOccurrenceDTO2);
    }
}
