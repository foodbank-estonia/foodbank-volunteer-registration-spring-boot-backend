package ee.foodbank.volunteerregistration.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import ee.foodbank.volunteerregistration.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class FoodbankRegionDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FoodbankRegionDTO.class);
        FoodbankRegionDTO foodbankRegionDTO1 = new FoodbankRegionDTO();
        foodbankRegionDTO1.setId(1L);
        FoodbankRegionDTO foodbankRegionDTO2 = new FoodbankRegionDTO();
        assertThat(foodbankRegionDTO1).isNotEqualTo(foodbankRegionDTO2);
        foodbankRegionDTO2.setId(foodbankRegionDTO1.getId());
        assertThat(foodbankRegionDTO1).isEqualTo(foodbankRegionDTO2);
        foodbankRegionDTO2.setId(2L);
        assertThat(foodbankRegionDTO1).isNotEqualTo(foodbankRegionDTO2);
        foodbankRegionDTO1.setId(null);
        assertThat(foodbankRegionDTO1).isNotEqualTo(foodbankRegionDTO2);
    }
}
