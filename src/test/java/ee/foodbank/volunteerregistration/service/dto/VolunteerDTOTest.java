package ee.foodbank.volunteerregistration.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import ee.foodbank.volunteerregistration.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class VolunteerDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(VolunteerDTO.class);
        VolunteerDTO volunteerDTO1 = new VolunteerDTO();
        volunteerDTO1.setId(1L);
        VolunteerDTO volunteerDTO2 = new VolunteerDTO();
        assertThat(volunteerDTO1).isNotEqualTo(volunteerDTO2);
        volunteerDTO2.setId(volunteerDTO1.getId());
        assertThat(volunteerDTO1).isEqualTo(volunteerDTO2);
        volunteerDTO2.setId(2L);
        assertThat(volunteerDTO1).isNotEqualTo(volunteerDTO2);
        volunteerDTO1.setId(null);
        assertThat(volunteerDTO1).isNotEqualTo(volunteerDTO2);
    }
}
