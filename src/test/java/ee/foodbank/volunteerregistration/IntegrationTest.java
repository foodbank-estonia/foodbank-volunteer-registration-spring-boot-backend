package ee.foodbank.volunteerregistration;

import ee.foodbank.volunteerregistration.FoodbankVolunteerRegistrationApp;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Base composite annotation for integration tests.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@SpringBootTest(classes = FoodbankVolunteerRegistrationApp.class)
public @interface IntegrationTest {
}
