package ee.foodbank.volunteerregistration.domain;

import static org.assertj.core.api.Assertions.assertThat;

import ee.foodbank.volunteerregistration.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class FoodbankRegionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FoodbankRegion.class);
        FoodbankRegion foodbankRegion1 = new FoodbankRegion();
        foodbankRegion1.setId(1L);
        FoodbankRegion foodbankRegion2 = new FoodbankRegion();
        foodbankRegion2.setId(foodbankRegion1.getId());
        assertThat(foodbankRegion1).isEqualTo(foodbankRegion2);
        foodbankRegion2.setId(2L);
        assertThat(foodbankRegion1).isNotEqualTo(foodbankRegion2);
        foodbankRegion1.setId(null);
        assertThat(foodbankRegion1).isNotEqualTo(foodbankRegion2);
    }
}
