package ee.foodbank.volunteerregistration.domain;

import static org.assertj.core.api.Assertions.assertThat;

import ee.foodbank.volunteerregistration.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ActivityOccurrenceTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ActivityOccurrence.class);
        ActivityOccurrence activityOccurrence1 = new ActivityOccurrence();
        activityOccurrence1.setId(1L);
        ActivityOccurrence activityOccurrence2 = new ActivityOccurrence();
        activityOccurrence2.setId(activityOccurrence1.getId());
        assertThat(activityOccurrence1).isEqualTo(activityOccurrence2);
        activityOccurrence2.setId(2L);
        assertThat(activityOccurrence1).isNotEqualTo(activityOccurrence2);
        activityOccurrence1.setId(null);
        assertThat(activityOccurrence1).isNotEqualTo(activityOccurrence2);
    }
}
