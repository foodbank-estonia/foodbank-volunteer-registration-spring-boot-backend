package ee.foodbank.volunteerregistration.domain;

import static org.assertj.core.api.Assertions.assertThat;

import ee.foodbank.volunteerregistration.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class VolunteerRegistrationTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VolunteerRegistration.class);
        VolunteerRegistration volunteerRegistration1 = new VolunteerRegistration();
        volunteerRegistration1.setId(1L);
        VolunteerRegistration volunteerRegistration2 = new VolunteerRegistration();
        volunteerRegistration2.setId(volunteerRegistration1.getId());
        assertThat(volunteerRegistration1).isEqualTo(volunteerRegistration2);
        volunteerRegistration2.setId(2L);
        assertThat(volunteerRegistration1).isNotEqualTo(volunteerRegistration2);
        volunteerRegistration1.setId(null);
        assertThat(volunteerRegistration1).isNotEqualTo(volunteerRegistration2);
    }
}
