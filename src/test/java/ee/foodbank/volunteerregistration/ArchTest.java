package ee.foodbank.volunteerregistration;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("ee.foodbank.volunteerregistration");

        noClasses()
            .that()
            .resideInAnyPackage("ee.foodbank.volunteerregistration.service..")
            .or()
            .resideInAnyPackage("ee.foodbank.volunteerregistration.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..ee.foodbank.volunteerregistration.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
