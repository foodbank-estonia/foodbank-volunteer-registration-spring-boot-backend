/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import * as config from '@/shared/config/config';
import TemplateUpdateComponent from '@/entities/template/template-update.vue';
import TemplateClass from '@/entities/template/template-update.component';
import TemplateService from '@/entities/template/template.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Template Management Update Component', () => {
    let wrapper: Wrapper<TemplateClass>;
    let comp: TemplateClass;
    let templateServiceStub: SinonStubbedInstance<TemplateService>;

    beforeEach(() => {
      templateServiceStub = sinon.createStubInstance<TemplateService>(TemplateService);

      wrapper = shallowMount<TemplateClass>(TemplateUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          templateService: () => templateServiceStub,
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.template = entity;
        templateServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(templateServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.template = entity;
        templateServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(templateServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundTemplate = { id: 123 };
        templateServiceStub.find.resolves(foundTemplate);
        templateServiceStub.retrieve.resolves([foundTemplate]);

        // WHEN
        comp.beforeRouteEnter({ params: { templateId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.template).toBe(foundTemplate);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
