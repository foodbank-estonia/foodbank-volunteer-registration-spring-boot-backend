/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import ActivityOccurrenceComponent from '@/entities/activity-occurrence/activity-occurrence.vue';
import ActivityOccurrenceClass from '@/entities/activity-occurrence/activity-occurrence.component';
import ActivityOccurrenceService from '@/entities/activity-occurrence/activity-occurrence.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('ActivityOccurrence Management Component', () => {
    let wrapper: Wrapper<ActivityOccurrenceClass>;
    let comp: ActivityOccurrenceClass;
    let activityOccurrenceServiceStub: SinonStubbedInstance<ActivityOccurrenceService>;

    beforeEach(() => {
      activityOccurrenceServiceStub = sinon.createStubInstance<ActivityOccurrenceService>(ActivityOccurrenceService);
      activityOccurrenceServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<ActivityOccurrenceClass>(ActivityOccurrenceComponent, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          activityOccurrenceService: () => activityOccurrenceServiceStub,
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      activityOccurrenceServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllActivityOccurrences();
      await comp.$nextTick();

      // THEN
      expect(activityOccurrenceServiceStub.retrieve.called).toBeTruthy();
      expect(comp.activityOccurrences[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      activityOccurrenceServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(activityOccurrenceServiceStub.retrieve.called).toBeTruthy();
      expect(comp.activityOccurrences[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should not load a page if the page is the same as the previous page', () => {
      // GIVEN
      activityOccurrenceServiceStub.retrieve.reset();
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(activityOccurrenceServiceStub.retrieve.called).toBeFalsy();
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      activityOccurrenceServiceStub.retrieve.reset();
      activityOccurrenceServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(activityOccurrenceServiceStub.retrieve.callCount).toEqual(3);
      expect(comp.page).toEqual(1);
      expect(comp.activityOccurrences[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      activityOccurrenceServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeActivityOccurrence();
      await comp.$nextTick();

      // THEN
      expect(activityOccurrenceServiceStub.delete.called).toBeTruthy();
      expect(activityOccurrenceServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});
