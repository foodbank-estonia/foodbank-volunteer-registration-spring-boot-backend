/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import ActivityOccurrenceDetailComponent from '@/entities/activity-occurrence/activity-occurrence-details.vue';
import ActivityOccurrenceClass from '@/entities/activity-occurrence/activity-occurrence-details.component';
import ActivityOccurrenceService from '@/entities/activity-occurrence/activity-occurrence.service';
import router from '@/router';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('ActivityOccurrence Management Detail Component', () => {
    let wrapper: Wrapper<ActivityOccurrenceClass>;
    let comp: ActivityOccurrenceClass;
    let activityOccurrenceServiceStub: SinonStubbedInstance<ActivityOccurrenceService>;

    beforeEach(() => {
      activityOccurrenceServiceStub = sinon.createStubInstance<ActivityOccurrenceService>(ActivityOccurrenceService);

      wrapper = shallowMount<ActivityOccurrenceClass>(ActivityOccurrenceDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { activityOccurrenceService: () => activityOccurrenceServiceStub },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundActivityOccurrence = { id: 123 };
        activityOccurrenceServiceStub.find.resolves(foundActivityOccurrence);

        // WHEN
        comp.retrieveActivityOccurrence(123);
        await comp.$nextTick();

        // THEN
        expect(comp.activityOccurrence).toBe(foundActivityOccurrence);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundActivityOccurrence = { id: 123 };
        activityOccurrenceServiceStub.find.resolves(foundActivityOccurrence);

        // WHEN
        comp.beforeRouteEnter({ params: { activityOccurrenceId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.activityOccurrence).toBe(foundActivityOccurrence);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
