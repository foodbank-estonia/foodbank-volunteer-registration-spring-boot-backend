/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import * as config from '@/shared/config/config';
import ActivityOccurrenceUpdateComponent from '@/entities/activity-occurrence/activity-occurrence-update.vue';
import ActivityOccurrenceClass from '@/entities/activity-occurrence/activity-occurrence-update.component';
import ActivityOccurrenceService from '@/entities/activity-occurrence/activity-occurrence.service';

import ActivityService from '@/entities/activity/activity.service';

import LocationService from '@/entities/location/location.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('ActivityOccurrence Management Update Component', () => {
    let wrapper: Wrapper<ActivityOccurrenceClass>;
    let comp: ActivityOccurrenceClass;
    let activityOccurrenceServiceStub: SinonStubbedInstance<ActivityOccurrenceService>;

    beforeEach(() => {
      activityOccurrenceServiceStub = sinon.createStubInstance<ActivityOccurrenceService>(ActivityOccurrenceService);

      wrapper = shallowMount<ActivityOccurrenceClass>(ActivityOccurrenceUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          activityOccurrenceService: () => activityOccurrenceServiceStub,

          activityService: () => new ActivityService(),

          locationService: () => new LocationService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('load', () => {
      it('Should convert date from string', () => {
        // GIVEN
        const date = new Date('2019-10-15T11:42:02Z');

        // WHEN
        const convertedDate = comp.convertDateTimeFromServer(date);

        // THEN
        expect(convertedDate).toEqual(dayjs(date).format(DATE_TIME_LONG_FORMAT));
      });

      it('Should not convert date if date is not present', () => {
        expect(comp.convertDateTimeFromServer(null)).toBeNull();
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.activityOccurrence = entity;
        activityOccurrenceServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(activityOccurrenceServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.activityOccurrence = entity;
        activityOccurrenceServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(activityOccurrenceServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundActivityOccurrence = { id: 123 };
        activityOccurrenceServiceStub.find.resolves(foundActivityOccurrence);
        activityOccurrenceServiceStub.retrieve.resolves([foundActivityOccurrence]);

        // WHEN
        comp.beforeRouteEnter({ params: { activityOccurrenceId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.activityOccurrence).toBe(foundActivityOccurrence);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
