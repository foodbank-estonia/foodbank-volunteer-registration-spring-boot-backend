/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import * as config from '@/shared/config/config';
import VolunteerUpdateComponent from '@/entities/volunteer/volunteer-update.vue';
import VolunteerClass from '@/entities/volunteer/volunteer-update.component';
import VolunteerService from '@/entities/volunteer/volunteer.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Volunteer Management Update Component', () => {
    let wrapper: Wrapper<VolunteerClass>;
    let comp: VolunteerClass;
    let volunteerServiceStub: SinonStubbedInstance<VolunteerService>;

    beforeEach(() => {
      volunteerServiceStub = sinon.createStubInstance<VolunteerService>(VolunteerService);

      wrapper = shallowMount<VolunteerClass>(VolunteerUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          volunteerService: () => volunteerServiceStub,
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.volunteer = entity;
        volunteerServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(volunteerServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.volunteer = entity;
        volunteerServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(volunteerServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundVolunteer = { id: 123 };
        volunteerServiceStub.find.resolves(foundVolunteer);
        volunteerServiceStub.retrieve.resolves([foundVolunteer]);

        // WHEN
        comp.beforeRouteEnter({ params: { volunteerId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.volunteer).toBe(foundVolunteer);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
