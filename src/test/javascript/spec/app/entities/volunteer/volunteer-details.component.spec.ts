/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import VolunteerDetailComponent from '@/entities/volunteer/volunteer-details.vue';
import VolunteerClass from '@/entities/volunteer/volunteer-details.component';
import VolunteerService from '@/entities/volunteer/volunteer.service';
import router from '@/router';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Volunteer Management Detail Component', () => {
    let wrapper: Wrapper<VolunteerClass>;
    let comp: VolunteerClass;
    let volunteerServiceStub: SinonStubbedInstance<VolunteerService>;

    beforeEach(() => {
      volunteerServiceStub = sinon.createStubInstance<VolunteerService>(VolunteerService);

      wrapper = shallowMount<VolunteerClass>(VolunteerDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { volunteerService: () => volunteerServiceStub },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundVolunteer = { id: 123 };
        volunteerServiceStub.find.resolves(foundVolunteer);

        // WHEN
        comp.retrieveVolunteer(123);
        await comp.$nextTick();

        // THEN
        expect(comp.volunteer).toBe(foundVolunteer);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundVolunteer = { id: 123 };
        volunteerServiceStub.find.resolves(foundVolunteer);

        // WHEN
        comp.beforeRouteEnter({ params: { volunteerId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.volunteer).toBe(foundVolunteer);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
