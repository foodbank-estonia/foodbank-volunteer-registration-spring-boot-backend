/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import * as config from '@/shared/config/config';
import ActivityUpdateComponent from '@/entities/activity/activity-update.vue';
import ActivityClass from '@/entities/activity/activity-update.component';
import ActivityService from '@/entities/activity/activity.service';

import FoodbankRegionService from '@/entities/foodbank-region/foodbank-region.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Activity Management Update Component', () => {
    let wrapper: Wrapper<ActivityClass>;
    let comp: ActivityClass;
    let activityServiceStub: SinonStubbedInstance<ActivityService>;

    beforeEach(() => {
      activityServiceStub = sinon.createStubInstance<ActivityService>(ActivityService);

      wrapper = shallowMount<ActivityClass>(ActivityUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          activityService: () => activityServiceStub,

          foodbankRegionService: () => new FoodbankRegionService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.activity = entity;
        activityServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(activityServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.activity = entity;
        activityServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(activityServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundActivity = { id: 123 };
        activityServiceStub.find.resolves(foundActivity);
        activityServiceStub.retrieve.resolves([foundActivity]);

        // WHEN
        comp.beforeRouteEnter({ params: { activityId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.activity).toBe(foundActivity);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
