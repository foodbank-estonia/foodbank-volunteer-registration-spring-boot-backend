/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import VolunteerRegistrationDetailComponent from '@/entities/volunteer-registration/volunteer-registration-details.vue';
import VolunteerRegistrationClass from '@/entities/volunteer-registration/volunteer-registration-details.component';
import VolunteerRegistrationService from '@/entities/volunteer-registration/volunteer-registration.service';
import router from '@/router';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('VolunteerRegistration Management Detail Component', () => {
    let wrapper: Wrapper<VolunteerRegistrationClass>;
    let comp: VolunteerRegistrationClass;
    let volunteerRegistrationServiceStub: SinonStubbedInstance<VolunteerRegistrationService>;

    beforeEach(() => {
      volunteerRegistrationServiceStub = sinon.createStubInstance<VolunteerRegistrationService>(VolunteerRegistrationService);

      wrapper = shallowMount<VolunteerRegistrationClass>(VolunteerRegistrationDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { volunteerRegistrationService: () => volunteerRegistrationServiceStub },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundVolunteerRegistration = { id: 123 };
        volunteerRegistrationServiceStub.find.resolves(foundVolunteerRegistration);

        // WHEN
        comp.retrieveVolunteerRegistration(123);
        await comp.$nextTick();

        // THEN
        expect(comp.volunteerRegistration).toBe(foundVolunteerRegistration);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundVolunteerRegistration = { id: 123 };
        volunteerRegistrationServiceStub.find.resolves(foundVolunteerRegistration);

        // WHEN
        comp.beforeRouteEnter({ params: { volunteerRegistrationId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.volunteerRegistration).toBe(foundVolunteerRegistration);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
