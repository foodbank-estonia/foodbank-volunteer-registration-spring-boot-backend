/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import * as config from '@/shared/config/config';
import VolunteerRegistrationUpdateComponent from '@/entities/volunteer-registration/volunteer-registration-update.vue';
import VolunteerRegistrationClass from '@/entities/volunteer-registration/volunteer-registration-update.component';
import VolunteerRegistrationService from '@/entities/volunteer-registration/volunteer-registration.service';

import ActivityOccurrenceService from '@/entities/activity-occurrence/activity-occurrence.service';

import VolunteerService from '@/entities/volunteer/volunteer.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('VolunteerRegistration Management Update Component', () => {
    let wrapper: Wrapper<VolunteerRegistrationClass>;
    let comp: VolunteerRegistrationClass;
    let volunteerRegistrationServiceStub: SinonStubbedInstance<VolunteerRegistrationService>;

    beforeEach(() => {
      volunteerRegistrationServiceStub = sinon.createStubInstance<VolunteerRegistrationService>(VolunteerRegistrationService);

      wrapper = shallowMount<VolunteerRegistrationClass>(VolunteerRegistrationUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          volunteerRegistrationService: () => volunteerRegistrationServiceStub,

          activityOccurrenceService: () => new ActivityOccurrenceService(),

          volunteerService: () => new VolunteerService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('load', () => {
      it('Should convert date from string', () => {
        // GIVEN
        const date = new Date('2019-10-15T11:42:02Z');

        // WHEN
        const convertedDate = comp.convertDateTimeFromServer(date);

        // THEN
        expect(convertedDate).toEqual(dayjs(date).format(DATE_TIME_LONG_FORMAT));
      });

      it('Should not convert date if date is not present', () => {
        expect(comp.convertDateTimeFromServer(null)).toBeNull();
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.volunteerRegistration = entity;
        volunteerRegistrationServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(volunteerRegistrationServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.volunteerRegistration = entity;
        volunteerRegistrationServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(volunteerRegistrationServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundVolunteerRegistration = { id: 123 };
        volunteerRegistrationServiceStub.find.resolves(foundVolunteerRegistration);
        volunteerRegistrationServiceStub.retrieve.resolves([foundVolunteerRegistration]);

        // WHEN
        comp.beforeRouteEnter({ params: { volunteerRegistrationId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.volunteerRegistration).toBe(foundVolunteerRegistration);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
