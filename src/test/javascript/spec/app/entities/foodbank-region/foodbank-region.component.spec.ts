/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import FoodbankRegionComponent from '@/entities/foodbank-region/foodbank-region.vue';
import FoodbankRegionClass from '@/entities/foodbank-region/foodbank-region.component';
import FoodbankRegionService from '@/entities/foodbank-region/foodbank-region.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('FoodbankRegion Management Component', () => {
    let wrapper: Wrapper<FoodbankRegionClass>;
    let comp: FoodbankRegionClass;
    let foodbankRegionServiceStub: SinonStubbedInstance<FoodbankRegionService>;

    beforeEach(() => {
      foodbankRegionServiceStub = sinon.createStubInstance<FoodbankRegionService>(FoodbankRegionService);
      foodbankRegionServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<FoodbankRegionClass>(FoodbankRegionComponent, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          foodbankRegionService: () => foodbankRegionServiceStub,
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      foodbankRegionServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllFoodbankRegions();
      await comp.$nextTick();

      // THEN
      expect(foodbankRegionServiceStub.retrieve.called).toBeTruthy();
      expect(comp.foodbankRegions[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      foodbankRegionServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(foodbankRegionServiceStub.retrieve.called).toBeTruthy();
      expect(comp.foodbankRegions[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should not load a page if the page is the same as the previous page', () => {
      // GIVEN
      foodbankRegionServiceStub.retrieve.reset();
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(foodbankRegionServiceStub.retrieve.called).toBeFalsy();
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      foodbankRegionServiceStub.retrieve.reset();
      foodbankRegionServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(foodbankRegionServiceStub.retrieve.callCount).toEqual(3);
      expect(comp.page).toEqual(1);
      expect(comp.foodbankRegions[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      foodbankRegionServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeFoodbankRegion();
      await comp.$nextTick();

      // THEN
      expect(foodbankRegionServiceStub.delete.called).toBeTruthy();
      expect(foodbankRegionServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});
