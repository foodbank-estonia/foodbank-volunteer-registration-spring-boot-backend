/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import FoodbankRegionDetailComponent from '@/entities/foodbank-region/foodbank-region-details.vue';
import FoodbankRegionClass from '@/entities/foodbank-region/foodbank-region-details.component';
import FoodbankRegionService from '@/entities/foodbank-region/foodbank-region.service';
import router from '@/router';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('FoodbankRegion Management Detail Component', () => {
    let wrapper: Wrapper<FoodbankRegionClass>;
    let comp: FoodbankRegionClass;
    let foodbankRegionServiceStub: SinonStubbedInstance<FoodbankRegionService>;

    beforeEach(() => {
      foodbankRegionServiceStub = sinon.createStubInstance<FoodbankRegionService>(FoodbankRegionService);

      wrapper = shallowMount<FoodbankRegionClass>(FoodbankRegionDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { foodbankRegionService: () => foodbankRegionServiceStub },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundFoodbankRegion = { id: 123 };
        foodbankRegionServiceStub.find.resolves(foundFoodbankRegion);

        // WHEN
        comp.retrieveFoodbankRegion(123);
        await comp.$nextTick();

        // THEN
        expect(comp.foodbankRegion).toBe(foundFoodbankRegion);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundFoodbankRegion = { id: 123 };
        foodbankRegionServiceStub.find.resolves(foundFoodbankRegion);

        // WHEN
        comp.beforeRouteEnter({ params: { foodbankRegionId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.foodbankRegion).toBe(foundFoodbankRegion);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
