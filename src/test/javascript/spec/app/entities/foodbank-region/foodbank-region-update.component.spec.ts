/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import * as config from '@/shared/config/config';
import FoodbankRegionUpdateComponent from '@/entities/foodbank-region/foodbank-region-update.vue';
import FoodbankRegionClass from '@/entities/foodbank-region/foodbank-region-update.component';
import FoodbankRegionService from '@/entities/foodbank-region/foodbank-region.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('FoodbankRegion Management Update Component', () => {
    let wrapper: Wrapper<FoodbankRegionClass>;
    let comp: FoodbankRegionClass;
    let foodbankRegionServiceStub: SinonStubbedInstance<FoodbankRegionService>;

    beforeEach(() => {
      foodbankRegionServiceStub = sinon.createStubInstance<FoodbankRegionService>(FoodbankRegionService);

      wrapper = shallowMount<FoodbankRegionClass>(FoodbankRegionUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          foodbankRegionService: () => foodbankRegionServiceStub,
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.foodbankRegion = entity;
        foodbankRegionServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(foodbankRegionServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.foodbankRegion = entity;
        foodbankRegionServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(foodbankRegionServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundFoodbankRegion = { id: 123 };
        foodbankRegionServiceStub.find.resolves(foundFoodbankRegion);
        foodbankRegionServiceStub.retrieve.resolves([foundFoodbankRegion]);

        // WHEN
        comp.beforeRouteEnter({ params: { foodbankRegionId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.foodbankRegion).toBe(foundFoodbankRegion);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
