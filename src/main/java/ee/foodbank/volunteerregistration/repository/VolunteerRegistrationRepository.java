package ee.foodbank.volunteerregistration.repository;

import ee.foodbank.volunteerregistration.domain.VolunteerRegistration;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the VolunteerRegistration entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VolunteerRegistrationRepository extends JpaRepository<VolunteerRegistration, Long> {}
