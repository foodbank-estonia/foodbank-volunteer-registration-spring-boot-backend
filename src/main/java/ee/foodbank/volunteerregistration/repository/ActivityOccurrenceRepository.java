package ee.foodbank.volunteerregistration.repository;

import ee.foodbank.volunteerregistration.domain.ActivityOccurrence;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ActivityOccurrence entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ActivityOccurrenceRepository extends JpaRepository<ActivityOccurrence, Long> {}
