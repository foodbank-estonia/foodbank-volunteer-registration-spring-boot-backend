package ee.foodbank.volunteerregistration.repository;

import ee.foodbank.volunteerregistration.domain.FoodbankRegion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the FoodbankRegion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FoodbankRegionRepository extends JpaRepository<FoodbankRegion, Long> {}
