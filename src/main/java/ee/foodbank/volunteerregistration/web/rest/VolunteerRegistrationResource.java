package ee.foodbank.volunteerregistration.web.rest;

import ee.foodbank.volunteerregistration.service.VolunteerRegistrationService;
import ee.foodbank.volunteerregistration.service.dto.VolunteerRegistrationDTO;
import ee.foodbank.volunteerregistration.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link ee.foodbank.volunteerregistration.domain.VolunteerRegistration}.
 */
@RestController
@RequestMapping("/api")
public class VolunteerRegistrationResource {

    private final Logger log = LoggerFactory.getLogger(VolunteerRegistrationResource.class);

    private static final String ENTITY_NAME = "volunteerRegistration";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VolunteerRegistrationService volunteerRegistrationService;

    public VolunteerRegistrationResource(VolunteerRegistrationService volunteerRegistrationService) {
        this.volunteerRegistrationService = volunteerRegistrationService;
    }

    /**
     * {@code POST  /volunteer-registrations} : Create a new volunteerRegistration.
     *
     * @param volunteerRegistrationDTO the volunteerRegistrationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new volunteerRegistrationDTO, or with status {@code 400 (Bad Request)} if the volunteerRegistration has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/volunteer-registrations")
    public ResponseEntity<VolunteerRegistrationDTO> createVolunteerRegistration(
        @Valid @RequestBody VolunteerRegistrationDTO volunteerRegistrationDTO
    ) throws URISyntaxException {
        log.debug("REST request to save VolunteerRegistration : {}", volunteerRegistrationDTO);
        if (volunteerRegistrationDTO.getId() != null) {
            throw new BadRequestAlertException("A new volunteerRegistration cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VolunteerRegistrationDTO result = volunteerRegistrationService.save(volunteerRegistrationDTO);
        return ResponseEntity
            .created(new URI("/api/volunteer-registrations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /volunteer-registrations} : Updates an existing volunteerRegistration.
     *
     * @param volunteerRegistrationDTO the volunteerRegistrationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated volunteerRegistrationDTO,
     * or with status {@code 400 (Bad Request)} if the volunteerRegistrationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the volunteerRegistrationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/volunteer-registrations")
    public ResponseEntity<VolunteerRegistrationDTO> updateVolunteerRegistration(
        @Valid @RequestBody VolunteerRegistrationDTO volunteerRegistrationDTO
    ) throws URISyntaxException {
        log.debug("REST request to update VolunteerRegistration : {}", volunteerRegistrationDTO);
        if (volunteerRegistrationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        VolunteerRegistrationDTO result = volunteerRegistrationService.save(volunteerRegistrationDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, volunteerRegistrationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /volunteer-registrations} : Updates given fields of an existing volunteerRegistration.
     *
     * @param volunteerRegistrationDTO the volunteerRegistrationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated volunteerRegistrationDTO,
     * or with status {@code 400 (Bad Request)} if the volunteerRegistrationDTO is not valid,
     * or with status {@code 404 (Not Found)} if the volunteerRegistrationDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the volunteerRegistrationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/volunteer-registrations", consumes = "application/merge-patch+json")
    public ResponseEntity<VolunteerRegistrationDTO> partialUpdateVolunteerRegistration(
        @NotNull @RequestBody VolunteerRegistrationDTO volunteerRegistrationDTO
    ) throws URISyntaxException {
        log.debug("REST request to update VolunteerRegistration partially : {}", volunteerRegistrationDTO);
        if (volunteerRegistrationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        Optional<VolunteerRegistrationDTO> result = volunteerRegistrationService.partialUpdate(volunteerRegistrationDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, volunteerRegistrationDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /volunteer-registrations} : get all the volunteerRegistrations.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of volunteerRegistrations in body.
     */
    @GetMapping("/volunteer-registrations")
    public ResponseEntity<List<VolunteerRegistrationDTO>> getAllVolunteerRegistrations(Pageable pageable) {
        log.debug("REST request to get a page of VolunteerRegistrations");
        Page<VolunteerRegistrationDTO> page = volunteerRegistrationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /volunteer-registrations/:id} : get the "id" volunteerRegistration.
     *
     * @param id the id of the volunteerRegistrationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the volunteerRegistrationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/volunteer-registrations/{id}")
    public ResponseEntity<VolunteerRegistrationDTO> getVolunteerRegistration(@PathVariable Long id) {
        log.debug("REST request to get VolunteerRegistration : {}", id);
        Optional<VolunteerRegistrationDTO> volunteerRegistrationDTO = volunteerRegistrationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(volunteerRegistrationDTO);
    }

    /**
     * {@code DELETE  /volunteer-registrations/:id} : delete the "id" volunteerRegistration.
     *
     * @param id the id of the volunteerRegistrationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/volunteer-registrations/{id}")
    public ResponseEntity<Void> deleteVolunteerRegistration(@PathVariable Long id) {
        log.debug("REST request to delete VolunteerRegistration : {}", id);
        volunteerRegistrationService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
