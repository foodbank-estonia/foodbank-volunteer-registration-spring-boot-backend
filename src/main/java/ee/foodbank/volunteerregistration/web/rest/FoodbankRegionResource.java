package ee.foodbank.volunteerregistration.web.rest;

import ee.foodbank.volunteerregistration.service.FoodbankRegionService;
import ee.foodbank.volunteerregistration.service.dto.FoodbankRegionDTO;
import ee.foodbank.volunteerregistration.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link ee.foodbank.volunteerregistration.domain.FoodbankRegion}.
 */
@RestController
@RequestMapping("/api")
public class FoodbankRegionResource {

    private final Logger log = LoggerFactory.getLogger(FoodbankRegionResource.class);

    private static final String ENTITY_NAME = "foodbankRegion";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FoodbankRegionService foodbankRegionService;

    public FoodbankRegionResource(FoodbankRegionService foodbankRegionService) {
        this.foodbankRegionService = foodbankRegionService;
    }

    /**
     * {@code POST  /foodbank-regions} : Create a new foodbankRegion.
     *
     * @param foodbankRegionDTO the foodbankRegionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new foodbankRegionDTO, or with status {@code 400 (Bad Request)} if the foodbankRegion has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/foodbank-regions")
    public ResponseEntity<FoodbankRegionDTO> createFoodbankRegion(@Valid @RequestBody FoodbankRegionDTO foodbankRegionDTO)
        throws URISyntaxException {
        log.debug("REST request to save FoodbankRegion : {}", foodbankRegionDTO);
        if (foodbankRegionDTO.getId() != null) {
            throw new BadRequestAlertException("A new foodbankRegion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FoodbankRegionDTO result = foodbankRegionService.save(foodbankRegionDTO);
        return ResponseEntity
            .created(new URI("/api/foodbank-regions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /foodbank-regions} : Updates an existing foodbankRegion.
     *
     * @param foodbankRegionDTO the foodbankRegionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated foodbankRegionDTO,
     * or with status {@code 400 (Bad Request)} if the foodbankRegionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the foodbankRegionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/foodbank-regions")
    public ResponseEntity<FoodbankRegionDTO> updateFoodbankRegion(@Valid @RequestBody FoodbankRegionDTO foodbankRegionDTO)
        throws URISyntaxException {
        log.debug("REST request to update FoodbankRegion : {}", foodbankRegionDTO);
        if (foodbankRegionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FoodbankRegionDTO result = foodbankRegionService.save(foodbankRegionDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, foodbankRegionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /foodbank-regions} : Updates given fields of an existing foodbankRegion.
     *
     * @param foodbankRegionDTO the foodbankRegionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated foodbankRegionDTO,
     * or with status {@code 400 (Bad Request)} if the foodbankRegionDTO is not valid,
     * or with status {@code 404 (Not Found)} if the foodbankRegionDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the foodbankRegionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/foodbank-regions", consumes = "application/merge-patch+json")
    public ResponseEntity<FoodbankRegionDTO> partialUpdateFoodbankRegion(@NotNull @RequestBody FoodbankRegionDTO foodbankRegionDTO)
        throws URISyntaxException {
        log.debug("REST request to update FoodbankRegion partially : {}", foodbankRegionDTO);
        if (foodbankRegionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        Optional<FoodbankRegionDTO> result = foodbankRegionService.partialUpdate(foodbankRegionDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, foodbankRegionDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /foodbank-regions} : get all the foodbankRegions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of foodbankRegions in body.
     */
    @GetMapping("/foodbank-regions")
    public ResponseEntity<List<FoodbankRegionDTO>> getAllFoodbankRegions(Pageable pageable) {
        log.debug("REST request to get a page of FoodbankRegions");
        Page<FoodbankRegionDTO> page = foodbankRegionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /foodbank-regions/:id} : get the "id" foodbankRegion.
     *
     * @param id the id of the foodbankRegionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the foodbankRegionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/foodbank-regions/{id}")
    public ResponseEntity<FoodbankRegionDTO> getFoodbankRegion(@PathVariable Long id) {
        log.debug("REST request to get FoodbankRegion : {}", id);
        Optional<FoodbankRegionDTO> foodbankRegionDTO = foodbankRegionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(foodbankRegionDTO);
    }

    /**
     * {@code DELETE  /foodbank-regions/:id} : delete the "id" foodbankRegion.
     *
     * @param id the id of the foodbankRegionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/foodbank-regions/{id}")
    public ResponseEntity<Void> deleteFoodbankRegion(@PathVariable Long id) {
        log.debug("REST request to delete FoodbankRegion : {}", id);
        foodbankRegionService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
