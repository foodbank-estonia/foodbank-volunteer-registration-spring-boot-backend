package ee.foodbank.volunteerregistration.web.rest;

import ee.foodbank.volunteerregistration.service.ActivityOccurrenceService;
import ee.foodbank.volunteerregistration.service.dto.ActivityOccurrenceDTO;
import ee.foodbank.volunteerregistration.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link ee.foodbank.volunteerregistration.domain.ActivityOccurrence}.
 */
@RestController
@RequestMapping("/api")
public class ActivityOccurrenceResource {

    private final Logger log = LoggerFactory.getLogger(ActivityOccurrenceResource.class);

    private static final String ENTITY_NAME = "activityOccurrence";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ActivityOccurrenceService activityOccurrenceService;

    public ActivityOccurrenceResource(ActivityOccurrenceService activityOccurrenceService) {
        this.activityOccurrenceService = activityOccurrenceService;
    }

    /**
     * {@code POST  /activity-occurrences} : Create a new activityOccurrence.
     *
     * @param activityOccurrenceDTO the activityOccurrenceDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new activityOccurrenceDTO, or with status {@code 400 (Bad Request)} if the activityOccurrence has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/activity-occurrences")
    public ResponseEntity<ActivityOccurrenceDTO> createActivityOccurrence(@Valid @RequestBody ActivityOccurrenceDTO activityOccurrenceDTO)
        throws URISyntaxException {
        log.debug("REST request to save ActivityOccurrence : {}", activityOccurrenceDTO);
        if (activityOccurrenceDTO.getId() != null) {
            throw new BadRequestAlertException("A new activityOccurrence cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ActivityOccurrenceDTO result = activityOccurrenceService.save(activityOccurrenceDTO);
        return ResponseEntity
            .created(new URI("/api/activity-occurrences/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /activity-occurrences} : Updates an existing activityOccurrence.
     *
     * @param activityOccurrenceDTO the activityOccurrenceDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated activityOccurrenceDTO,
     * or with status {@code 400 (Bad Request)} if the activityOccurrenceDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the activityOccurrenceDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/activity-occurrences")
    public ResponseEntity<ActivityOccurrenceDTO> updateActivityOccurrence(@Valid @RequestBody ActivityOccurrenceDTO activityOccurrenceDTO)
        throws URISyntaxException {
        log.debug("REST request to update ActivityOccurrence : {}", activityOccurrenceDTO);
        if (activityOccurrenceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ActivityOccurrenceDTO result = activityOccurrenceService.save(activityOccurrenceDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, activityOccurrenceDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /activity-occurrences} : Updates given fields of an existing activityOccurrence.
     *
     * @param activityOccurrenceDTO the activityOccurrenceDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated activityOccurrenceDTO,
     * or with status {@code 400 (Bad Request)} if the activityOccurrenceDTO is not valid,
     * or with status {@code 404 (Not Found)} if the activityOccurrenceDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the activityOccurrenceDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/activity-occurrences", consumes = "application/merge-patch+json")
    public ResponseEntity<ActivityOccurrenceDTO> partialUpdateActivityOccurrence(
        @NotNull @RequestBody ActivityOccurrenceDTO activityOccurrenceDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ActivityOccurrence partially : {}", activityOccurrenceDTO);
        if (activityOccurrenceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        Optional<ActivityOccurrenceDTO> result = activityOccurrenceService.partialUpdate(activityOccurrenceDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, activityOccurrenceDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /activity-occurrences} : get all the activityOccurrences.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of activityOccurrences in body.
     */
    @GetMapping("/activity-occurrences")
    public ResponseEntity<List<ActivityOccurrenceDTO>> getAllActivityOccurrences(Pageable pageable) {
        log.debug("REST request to get a page of ActivityOccurrences");
        Page<ActivityOccurrenceDTO> page = activityOccurrenceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /activity-occurrences/:id} : get the "id" activityOccurrence.
     *
     * @param id the id of the activityOccurrenceDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the activityOccurrenceDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/activity-occurrences/{id}")
    public ResponseEntity<ActivityOccurrenceDTO> getActivityOccurrence(@PathVariable Long id) {
        log.debug("REST request to get ActivityOccurrence : {}", id);
        Optional<ActivityOccurrenceDTO> activityOccurrenceDTO = activityOccurrenceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(activityOccurrenceDTO);
    }

    /**
     * {@code DELETE  /activity-occurrences/:id} : delete the "id" activityOccurrence.
     *
     * @param id the id of the activityOccurrenceDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/activity-occurrences/{id}")
    public ResponseEntity<Void> deleteActivityOccurrence(@PathVariable Long id) {
        log.debug("REST request to delete ActivityOccurrence : {}", id);
        activityOccurrenceService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
