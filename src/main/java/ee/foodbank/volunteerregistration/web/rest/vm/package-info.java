/**
 * View Models used by Spring MVC REST controllers.
 */
package ee.foodbank.volunteerregistration.web.rest.vm;
