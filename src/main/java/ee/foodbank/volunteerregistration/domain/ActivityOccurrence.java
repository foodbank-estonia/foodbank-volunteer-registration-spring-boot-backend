package ee.foodbank.volunteerregistration.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ActivityOccurrence.
 */
@Entity
@Table(name = "activity_occurrence")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ActivityOccurrence implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "start_time", nullable = false)
    private ZonedDateTime startTime;

    @NotNull
    @Column(name = "end_time", nullable = false)
    private ZonedDateTime endTime;

    @NotNull
    @Min(value = 1)
    @Max(value = 50)
    @Column(name = "max_volunteers", nullable = false)
    private Integer maxVolunteers;

    @NotNull
    @Min(value = 0)
    @Max(value = 50)
    @Column(name = "registered_volunteers", nullable = false)
    private Integer registeredVolunteers;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "foodbankRegion" }, allowSetters = true)
    private Activity activity;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "foodbankRegion" }, allowSetters = true)
    private Location location;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ActivityOccurrence id(Long id) {
        this.id = id;
        return this;
    }

    public ZonedDateTime getStartTime() {
        return this.startTime;
    }

    public ActivityOccurrence startTime(ZonedDateTime startTime) {
        this.startTime = startTime;
        return this;
    }

    public void setStartTime(ZonedDateTime startTime) {
        this.startTime = startTime;
    }

    public ZonedDateTime getEndTime() {
        return this.endTime;
    }

    public ActivityOccurrence endTime(ZonedDateTime endTime) {
        this.endTime = endTime;
        return this;
    }

    public void setEndTime(ZonedDateTime endTime) {
        this.endTime = endTime;
    }

    public Integer getMaxVolunteers() {
        return this.maxVolunteers;
    }

    public ActivityOccurrence maxVolunteers(Integer maxVolunteers) {
        this.maxVolunteers = maxVolunteers;
        return this;
    }

    public void setMaxVolunteers(Integer maxVolunteers) {
        this.maxVolunteers = maxVolunteers;
    }

    public Integer getRegisteredVolunteers() {
        return this.registeredVolunteers;
    }

    public ActivityOccurrence registeredVolunteers(Integer registeredVolunteers) {
        this.registeredVolunteers = registeredVolunteers;
        return this;
    }

    public void setRegisteredVolunteers(Integer registeredVolunteers) {
        this.registeredVolunteers = registeredVolunteers;
    }

    public Activity getActivity() {
        return this.activity;
    }

    public ActivityOccurrence activity(Activity activity) {
        this.setActivity(activity);
        return this;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public Location getLocation() {
        return this.location;
    }

    public ActivityOccurrence location(Location location) {
        this.setLocation(location);
        return this;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ActivityOccurrence)) {
            return false;
        }
        return id != null && id.equals(((ActivityOccurrence) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ActivityOccurrence{" +
            "id=" + getId() +
            ", startTime='" + getStartTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            ", maxVolunteers=" + getMaxVolunteers() +
            ", registeredVolunteers=" + getRegisteredVolunteers() +
            "}";
    }
}
