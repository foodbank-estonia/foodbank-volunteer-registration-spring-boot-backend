package ee.foodbank.volunteerregistration.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A VolunteerRegistration.
 */
@Entity
@Table(name = "volunteer_registration")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class VolunteerRegistration implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "registration_time", nullable = false)
    private ZonedDateTime registrationTime;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "activity", "location" }, allowSetters = true)
    private ActivityOccurrence activityOccurrence;

    @ManyToOne(optional = false)
    @NotNull
    private Volunteer volunteer;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VolunteerRegistration id(Long id) {
        this.id = id;
        return this;
    }

    public ZonedDateTime getRegistrationTime() {
        return this.registrationTime;
    }

    public VolunteerRegistration registrationTime(ZonedDateTime registrationTime) {
        this.registrationTime = registrationTime;
        return this;
    }

    public void setRegistrationTime(ZonedDateTime registrationTime) {
        this.registrationTime = registrationTime;
    }

    public ActivityOccurrence getActivityOccurrence() {
        return this.activityOccurrence;
    }

    public VolunteerRegistration activityOccurrence(ActivityOccurrence activityOccurrence) {
        this.setActivityOccurrence(activityOccurrence);
        return this;
    }

    public void setActivityOccurrence(ActivityOccurrence activityOccurrence) {
        this.activityOccurrence = activityOccurrence;
    }

    public Volunteer getVolunteer() {
        return this.volunteer;
    }

    public VolunteerRegistration volunteer(Volunteer volunteer) {
        this.setVolunteer(volunteer);
        return this;
    }

    public void setVolunteer(Volunteer volunteer) {
        this.volunteer = volunteer;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VolunteerRegistration)) {
            return false;
        }
        return id != null && id.equals(((VolunteerRegistration) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VolunteerRegistration{" +
            "id=" + getId() +
            ", registrationTime='" + getRegistrationTime() + "'" +
            "}";
    }
}
