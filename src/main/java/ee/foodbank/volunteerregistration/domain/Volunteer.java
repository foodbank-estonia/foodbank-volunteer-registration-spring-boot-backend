package ee.foodbank.volunteerregistration.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Volunteer.
 */
@Entity
@Table(name = "volunteer")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Volunteer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(min = 2, max = 100)
    @Column(name = "first_name", length = 100, nullable = false)
    private String firstName;

    @NotNull
    @Size(min = 2, max = 100)
    @Column(name = "last_name", length = 100, nullable = false)
    private String lastName;

    @NotNull
    @Min(value = 5)
    @Max(value = 100)
    @Column(name = "age", nullable = false)
    private Integer age;

    @NotNull
    @Size(min = 4, max = 21)
    @Pattern(regexp = "^\\+?\\d{4,20}$")
    @Column(name = "phone", length = 21, nullable = false)
    private String phone;

    @NotNull
    @Size(min = 6, max = 254)
    @Pattern(regexp = "^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$")
    @Column(name = "email", length = 254, nullable = false)
    private String email;

    @NotNull
    @Column(name = "is_group", nullable = false)
    private Boolean isGroup;

    @Size(min = 2, max = 100)
    @Column(name = "group_name", length = 100)
    private String groupName;

    @Min(value = 2)
    @Max(value = 50)
    @Column(name = "group_participant_count")
    private Integer groupParticipantCount;

    @Lob
    @Column(name = "notes")
    private String notes;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Volunteer id(Long id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public Volunteer firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public Volunteer lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return this.age;
    }

    public Volunteer age(Integer age) {
        this.age = age;
        return this;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPhone() {
        return this.phone;
    }

    public Volunteer phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return this.email;
    }

    public Volunteer email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIsGroup() {
        return this.isGroup;
    }

    public Volunteer isGroup(Boolean isGroup) {
        this.isGroup = isGroup;
        return this;
    }

    public void setIsGroup(Boolean isGroup) {
        this.isGroup = isGroup;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public Volunteer groupName(String groupName) {
        this.groupName = groupName;
        return this;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getGroupParticipantCount() {
        return this.groupParticipantCount;
    }

    public Volunteer groupParticipantCount(Integer groupParticipantCount) {
        this.groupParticipantCount = groupParticipantCount;
        return this;
    }

    public void setGroupParticipantCount(Integer groupParticipantCount) {
        this.groupParticipantCount = groupParticipantCount;
    }

    public String getNotes() {
        return this.notes;
    }

    public Volunteer notes(String notes) {
        this.notes = notes;
        return this;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Volunteer)) {
            return false;
        }
        return id != null && id.equals(((Volunteer) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Volunteer{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", age=" + getAge() +
            ", phone='" + getPhone() + "'" +
            ", email='" + getEmail() + "'" +
            ", isGroup='" + getIsGroup() + "'" +
            ", groupName='" + getGroupName() + "'" +
            ", groupParticipantCount=" + getGroupParticipantCount() +
            ", notes='" + getNotes() + "'" +
            "}";
    }
}
