package ee.foodbank.volunteerregistration.domain.enumeration;

/**
 * The GroupType enumeration.
 */
public enum GroupType {
    ONE,
    MORE_THAN_ONE,
    BOTH,
}
