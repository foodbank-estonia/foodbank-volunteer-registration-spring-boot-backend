package ee.foodbank.volunteerregistration.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Template.
 */
@Entity
@Table(name = "template")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Template implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    @Column(name = "page_header", nullable = false)
    private String pageHeader;

    @Lob
    @Column(name = "page_footer", nullable = false)
    private String pageFooter;

    @Lob
    @Column(name = "quality_badge_text", nullable = false)
    private String qualityBadgeText;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Template id(Long id) {
        this.id = id;
        return this;
    }

    public String getPageHeader() {
        return this.pageHeader;
    }

    public Template pageHeader(String pageHeader) {
        this.pageHeader = pageHeader;
        return this;
    }

    public void setPageHeader(String pageHeader) {
        this.pageHeader = pageHeader;
    }

    public String getPageFooter() {
        return this.pageFooter;
    }

    public Template pageFooter(String pageFooter) {
        this.pageFooter = pageFooter;
        return this;
    }

    public void setPageFooter(String pageFooter) {
        this.pageFooter = pageFooter;
    }

    public String getQualityBadgeText() {
        return this.qualityBadgeText;
    }

    public Template qualityBadgeText(String qualityBadgeText) {
        this.qualityBadgeText = qualityBadgeText;
        return this;
    }

    public void setQualityBadgeText(String qualityBadgeText) {
        this.qualityBadgeText = qualityBadgeText;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Template)) {
            return false;
        }
        return id != null && id.equals(((Template) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Template{" +
            "id=" + getId() +
            ", pageHeader='" + getPageHeader() + "'" +
            ", pageFooter='" + getPageFooter() + "'" +
            ", qualityBadgeText='" + getQualityBadgeText() + "'" +
            "}";
    }
}
