package ee.foodbank.volunteerregistration.service.mapper;

import ee.foodbank.volunteerregistration.domain.*;
import ee.foodbank.volunteerregistration.service.dto.VolunteerRegistrationDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link VolunteerRegistration} and its DTO {@link VolunteerRegistrationDTO}.
 */
@Mapper(componentModel = "spring", uses = { ActivityOccurrenceMapper.class, VolunteerMapper.class })
public interface VolunteerRegistrationMapper extends EntityMapper<VolunteerRegistrationDTO, VolunteerRegistration> {
    @Mapping(target = "activityOccurrence", source = "activityOccurrence", qualifiedByName = "startTime")
    @Mapping(target = "volunteer", source = "volunteer", qualifiedByName = "email")
    VolunteerRegistrationDTO toDto(VolunteerRegistration s);
}
