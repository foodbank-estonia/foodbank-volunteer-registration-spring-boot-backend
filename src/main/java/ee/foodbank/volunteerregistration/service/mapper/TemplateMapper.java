package ee.foodbank.volunteerregistration.service.mapper;

import ee.foodbank.volunteerregistration.domain.*;
import ee.foodbank.volunteerregistration.service.dto.TemplateDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Template} and its DTO {@link TemplateDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TemplateMapper extends EntityMapper<TemplateDTO, Template> {}
