package ee.foodbank.volunteerregistration.service.mapper;

import ee.foodbank.volunteerregistration.domain.*;
import ee.foodbank.volunteerregistration.service.dto.VolunteerDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Volunteer} and its DTO {@link VolunteerDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface VolunteerMapper extends EntityMapper<VolunteerDTO, Volunteer> {
    @Named("email")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "email", source = "email")
    VolunteerDTO toDtoEmail(Volunteer volunteer);
}
