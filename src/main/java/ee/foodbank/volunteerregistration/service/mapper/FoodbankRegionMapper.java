package ee.foodbank.volunteerregistration.service.mapper;

import ee.foodbank.volunteerregistration.domain.*;
import ee.foodbank.volunteerregistration.service.dto.FoodbankRegionDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link FoodbankRegion} and its DTO {@link FoodbankRegionDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FoodbankRegionMapper extends EntityMapper<FoodbankRegionDTO, FoodbankRegion> {
    @Named("name")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    FoodbankRegionDTO toDtoName(FoodbankRegion foodbankRegion);
}
