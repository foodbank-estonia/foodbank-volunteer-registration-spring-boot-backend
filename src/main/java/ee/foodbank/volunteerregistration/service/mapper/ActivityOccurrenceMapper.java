package ee.foodbank.volunteerregistration.service.mapper;

import ee.foodbank.volunteerregistration.domain.*;
import ee.foodbank.volunteerregistration.service.dto.ActivityOccurrenceDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ActivityOccurrence} and its DTO {@link ActivityOccurrenceDTO}.
 */
@Mapper(componentModel = "spring", uses = { ActivityMapper.class, LocationMapper.class })
public interface ActivityOccurrenceMapper extends EntityMapper<ActivityOccurrenceDTO, ActivityOccurrence> {
    @Mapping(target = "activity", source = "activity", qualifiedByName = "name")
    @Mapping(target = "location", source = "location", qualifiedByName = "name")
    ActivityOccurrenceDTO toDto(ActivityOccurrence s);

    @Named("startTime")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "startTime", source = "startTime")
    ActivityOccurrenceDTO toDtoStartTime(ActivityOccurrence activityOccurrence);
}
