package ee.foodbank.volunteerregistration.service;

import ee.foodbank.volunteerregistration.domain.ActivityOccurrence;
import ee.foodbank.volunteerregistration.repository.ActivityOccurrenceRepository;
import ee.foodbank.volunteerregistration.service.dto.ActivityOccurrenceDTO;
import ee.foodbank.volunteerregistration.service.mapper.ActivityOccurrenceMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ActivityOccurrence}.
 */
@Service
@Transactional
public class ActivityOccurrenceService {

    private final Logger log = LoggerFactory.getLogger(ActivityOccurrenceService.class);

    private final ActivityOccurrenceRepository activityOccurrenceRepository;

    private final ActivityOccurrenceMapper activityOccurrenceMapper;

    public ActivityOccurrenceService(
        ActivityOccurrenceRepository activityOccurrenceRepository,
        ActivityOccurrenceMapper activityOccurrenceMapper
    ) {
        this.activityOccurrenceRepository = activityOccurrenceRepository;
        this.activityOccurrenceMapper = activityOccurrenceMapper;
    }

    /**
     * Save a activityOccurrence.
     *
     * @param activityOccurrenceDTO the entity to save.
     * @return the persisted entity.
     */
    public ActivityOccurrenceDTO save(ActivityOccurrenceDTO activityOccurrenceDTO) {
        log.debug("Request to save ActivityOccurrence : {}", activityOccurrenceDTO);
        ActivityOccurrence activityOccurrence = activityOccurrenceMapper.toEntity(activityOccurrenceDTO);
        activityOccurrence = activityOccurrenceRepository.save(activityOccurrence);
        return activityOccurrenceMapper.toDto(activityOccurrence);
    }

    /**
     * Partially update a activityOccurrence.
     *
     * @param activityOccurrenceDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ActivityOccurrenceDTO> partialUpdate(ActivityOccurrenceDTO activityOccurrenceDTO) {
        log.debug("Request to partially update ActivityOccurrence : {}", activityOccurrenceDTO);

        return activityOccurrenceRepository
            .findById(activityOccurrenceDTO.getId())
            .map(
                existingActivityOccurrence -> {
                    if (activityOccurrenceDTO.getStartTime() != null) {
                        existingActivityOccurrence.setStartTime(activityOccurrenceDTO.getStartTime());
                    }

                    if (activityOccurrenceDTO.getEndTime() != null) {
                        existingActivityOccurrence.setEndTime(activityOccurrenceDTO.getEndTime());
                    }

                    if (activityOccurrenceDTO.getMaxVolunteers() != null) {
                        existingActivityOccurrence.setMaxVolunteers(activityOccurrenceDTO.getMaxVolunteers());
                    }

                    if (activityOccurrenceDTO.getRegisteredVolunteers() != null) {
                        existingActivityOccurrence.setRegisteredVolunteers(activityOccurrenceDTO.getRegisteredVolunteers());
                    }

                    return existingActivityOccurrence;
                }
            )
            .map(activityOccurrenceRepository::save)
            .map(activityOccurrenceMapper::toDto);
    }

    /**
     * Get all the activityOccurrences.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ActivityOccurrenceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ActivityOccurrences");
        return activityOccurrenceRepository.findAll(pageable).map(activityOccurrenceMapper::toDto);
    }

    /**
     * Get one activityOccurrence by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ActivityOccurrenceDTO> findOne(Long id) {
        log.debug("Request to get ActivityOccurrence : {}", id);
        return activityOccurrenceRepository.findById(id).map(activityOccurrenceMapper::toDto);
    }

    /**
     * Delete the activityOccurrence by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ActivityOccurrence : {}", id);
        activityOccurrenceRepository.deleteById(id);
    }
}
