package ee.foodbank.volunteerregistration.service;

import ee.foodbank.volunteerregistration.domain.FoodbankRegion;
import ee.foodbank.volunteerregistration.repository.FoodbankRegionRepository;
import ee.foodbank.volunteerregistration.service.dto.FoodbankRegionDTO;
import ee.foodbank.volunteerregistration.service.mapper.FoodbankRegionMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link FoodbankRegion}.
 */
@Service
@Transactional
public class FoodbankRegionService {

    private final Logger log = LoggerFactory.getLogger(FoodbankRegionService.class);

    private final FoodbankRegionRepository foodbankRegionRepository;

    private final FoodbankRegionMapper foodbankRegionMapper;

    public FoodbankRegionService(FoodbankRegionRepository foodbankRegionRepository, FoodbankRegionMapper foodbankRegionMapper) {
        this.foodbankRegionRepository = foodbankRegionRepository;
        this.foodbankRegionMapper = foodbankRegionMapper;
    }

    /**
     * Save a foodbankRegion.
     *
     * @param foodbankRegionDTO the entity to save.
     * @return the persisted entity.
     */
    public FoodbankRegionDTO save(FoodbankRegionDTO foodbankRegionDTO) {
        log.debug("Request to save FoodbankRegion : {}", foodbankRegionDTO);
        FoodbankRegion foodbankRegion = foodbankRegionMapper.toEntity(foodbankRegionDTO);
        foodbankRegion = foodbankRegionRepository.save(foodbankRegion);
        return foodbankRegionMapper.toDto(foodbankRegion);
    }

    /**
     * Partially update a foodbankRegion.
     *
     * @param foodbankRegionDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<FoodbankRegionDTO> partialUpdate(FoodbankRegionDTO foodbankRegionDTO) {
        log.debug("Request to partially update FoodbankRegion : {}", foodbankRegionDTO);

        return foodbankRegionRepository
            .findById(foodbankRegionDTO.getId())
            .map(
                existingFoodbankRegion -> {
                    if (foodbankRegionDTO.getName() != null) {
                        existingFoodbankRegion.setName(foodbankRegionDTO.getName());
                    }

                    if (foodbankRegionDTO.getDescription() != null) {
                        existingFoodbankRegion.setDescription(foodbankRegionDTO.getDescription());
                    }

                    if (foodbankRegionDTO.getQualityBadge() != null) {
                        existingFoodbankRegion.setQualityBadge(foodbankRegionDTO.getQualityBadge());
                    }

                    return existingFoodbankRegion;
                }
            )
            .map(foodbankRegionRepository::save)
            .map(foodbankRegionMapper::toDto);
    }

    /**
     * Get all the foodbankRegions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<FoodbankRegionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all FoodbankRegions");
        return foodbankRegionRepository.findAll(pageable).map(foodbankRegionMapper::toDto);
    }

    /**
     * Get one foodbankRegion by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<FoodbankRegionDTO> findOne(Long id) {
        log.debug("Request to get FoodbankRegion : {}", id);
        return foodbankRegionRepository.findById(id).map(foodbankRegionMapper::toDto);
    }

    /**
     * Delete the foodbankRegion by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete FoodbankRegion : {}", id);
        foodbankRegionRepository.deleteById(id);
    }
}
