package ee.foodbank.volunteerregistration.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link ee.foodbank.volunteerregistration.domain.Volunteer} entity.
 */
public class VolunteerDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 2, max = 100)
    private String firstName;

    @NotNull
    @Size(min = 2, max = 100)
    private String lastName;

    @NotNull
    @Min(value = 5)
    @Max(value = 100)
    private Integer age;

    @NotNull
    @Size(min = 4, max = 21)
    @Pattern(regexp = "^\\+?\\d{4,20}$")
    private String phone;

    @NotNull
    @Size(min = 6, max = 254)
    @Pattern(regexp = "^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$")
    private String email;

    @NotNull
    private Boolean isGroup;

    @Size(min = 2, max = 100)
    private String groupName;

    @Min(value = 2)
    @Max(value = 50)
    private Integer groupParticipantCount;

    @Lob
    private String notes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIsGroup() {
        return isGroup;
    }

    public void setIsGroup(Boolean isGroup) {
        this.isGroup = isGroup;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getGroupParticipantCount() {
        return groupParticipantCount;
    }

    public void setGroupParticipantCount(Integer groupParticipantCount) {
        this.groupParticipantCount = groupParticipantCount;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VolunteerDTO)) {
            return false;
        }

        VolunteerDTO volunteerDTO = (VolunteerDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, volunteerDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VolunteerDTO{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", age=" + getAge() +
            ", phone='" + getPhone() + "'" +
            ", email='" + getEmail() + "'" +
            ", isGroup='" + getIsGroup() + "'" +
            ", groupName='" + getGroupName() + "'" +
            ", groupParticipantCount=" + getGroupParticipantCount() +
            ", notes='" + getNotes() + "'" +
            "}";
    }
}
