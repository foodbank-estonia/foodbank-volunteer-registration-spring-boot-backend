package ee.foodbank.volunteerregistration.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link ee.foodbank.volunteerregistration.domain.VolunteerRegistration} entity.
 */
public class VolunteerRegistrationDTO implements Serializable {

    private Long id;

    @NotNull
    private ZonedDateTime registrationTime;

    private ActivityOccurrenceDTO activityOccurrence;

    private VolunteerDTO volunteer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getRegistrationTime() {
        return registrationTime;
    }

    public void setRegistrationTime(ZonedDateTime registrationTime) {
        this.registrationTime = registrationTime;
    }

    public ActivityOccurrenceDTO getActivityOccurrence() {
        return activityOccurrence;
    }

    public void setActivityOccurrence(ActivityOccurrenceDTO activityOccurrence) {
        this.activityOccurrence = activityOccurrence;
    }

    public VolunteerDTO getVolunteer() {
        return volunteer;
    }

    public void setVolunteer(VolunteerDTO volunteer) {
        this.volunteer = volunteer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VolunteerRegistrationDTO)) {
            return false;
        }

        VolunteerRegistrationDTO volunteerRegistrationDTO = (VolunteerRegistrationDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, volunteerRegistrationDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VolunteerRegistrationDTO{" +
            "id=" + getId() +
            ", registrationTime='" + getRegistrationTime() + "'" +
            ", activityOccurrence=" + getActivityOccurrence() +
            ", volunteer=" + getVolunteer() +
            "}";
    }
}
