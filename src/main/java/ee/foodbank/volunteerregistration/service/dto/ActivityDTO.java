package ee.foodbank.volunteerregistration.service.dto;

import ee.foodbank.volunteerregistration.domain.enumeration.GroupType;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link ee.foodbank.volunteerregistration.domain.Activity} entity.
 */
public class ActivityDTO implements Serializable {

    private Long id;

    @NotNull
    private GroupType group;

    @NotNull
    @Size(min = 2, max = 255)
    private String name;

    @Lob
    private String description;

    @Lob
    private byte[] picture;

    private String pictureContentType;
    private FoodbankRegionDTO foodbankRegion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GroupType getGroup() {
        return group;
    }

    public void setGroup(GroupType group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getPictureContentType() {
        return pictureContentType;
    }

    public void setPictureContentType(String pictureContentType) {
        this.pictureContentType = pictureContentType;
    }

    public FoodbankRegionDTO getFoodbankRegion() {
        return foodbankRegion;
    }

    public void setFoodbankRegion(FoodbankRegionDTO foodbankRegion) {
        this.foodbankRegion = foodbankRegion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ActivityDTO)) {
            return false;
        }

        ActivityDTO activityDTO = (ActivityDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, activityDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ActivityDTO{" +
            "id=" + getId() +
            ", group='" + getGroup() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", picture='" + getPicture() + "'" +
            ", foodbankRegion=" + getFoodbankRegion() +
            "}";
    }
}
