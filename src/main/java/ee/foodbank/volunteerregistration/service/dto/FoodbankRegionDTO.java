package ee.foodbank.volunteerregistration.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link ee.foodbank.volunteerregistration.domain.FoodbankRegion} entity.
 */
public class FoodbankRegionDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 2, max = 255)
    private String name;

    @Lob
    private String description;

    private Boolean qualityBadge;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getQualityBadge() {
        return qualityBadge;
    }

    public void setQualityBadge(Boolean qualityBadge) {
        this.qualityBadge = qualityBadge;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FoodbankRegionDTO)) {
            return false;
        }

        FoodbankRegionDTO foodbankRegionDTO = (FoodbankRegionDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, foodbankRegionDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FoodbankRegionDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", qualityBadge='" + getQualityBadge() + "'" +
            "}";
    }
}
