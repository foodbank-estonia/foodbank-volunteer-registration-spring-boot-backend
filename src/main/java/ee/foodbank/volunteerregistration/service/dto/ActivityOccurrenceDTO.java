package ee.foodbank.volunteerregistration.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link ee.foodbank.volunteerregistration.domain.ActivityOccurrence} entity.
 */
public class ActivityOccurrenceDTO implements Serializable {

    private Long id;

    @NotNull
    private ZonedDateTime startTime;

    @NotNull
    private ZonedDateTime endTime;

    @NotNull
    @Min(value = 1)
    @Max(value = 50)
    private Integer maxVolunteers;

    @NotNull
    @Min(value = 0)
    @Max(value = 50)
    private Integer registeredVolunteers;

    private ActivityDTO activity;

    private LocationDTO location;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(ZonedDateTime startTime) {
        this.startTime = startTime;
    }

    public ZonedDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(ZonedDateTime endTime) {
        this.endTime = endTime;
    }

    public Integer getMaxVolunteers() {
        return maxVolunteers;
    }

    public void setMaxVolunteers(Integer maxVolunteers) {
        this.maxVolunteers = maxVolunteers;
    }

    public Integer getRegisteredVolunteers() {
        return registeredVolunteers;
    }

    public void setRegisteredVolunteers(Integer registeredVolunteers) {
        this.registeredVolunteers = registeredVolunteers;
    }

    public ActivityDTO getActivity() {
        return activity;
    }

    public void setActivity(ActivityDTO activity) {
        this.activity = activity;
    }

    public LocationDTO getLocation() {
        return location;
    }

    public void setLocation(LocationDTO location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ActivityOccurrenceDTO)) {
            return false;
        }

        ActivityOccurrenceDTO activityOccurrenceDTO = (ActivityOccurrenceDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, activityOccurrenceDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ActivityOccurrenceDTO{" +
            "id=" + getId() +
            ", startTime='" + getStartTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            ", maxVolunteers=" + getMaxVolunteers() +
            ", registeredVolunteers=" + getRegisteredVolunteers() +
            ", activity=" + getActivity() +
            ", location=" + getLocation() +
            "}";
    }
}
