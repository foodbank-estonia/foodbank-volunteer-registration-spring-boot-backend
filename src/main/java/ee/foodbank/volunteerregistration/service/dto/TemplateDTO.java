package ee.foodbank.volunteerregistration.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link ee.foodbank.volunteerregistration.domain.Template} entity.
 */
public class TemplateDTO implements Serializable {

    private Long id;

    @Lob
    private String pageHeader;

    @Lob
    private String pageFooter;

    @Lob
    private String qualityBadgeText;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPageHeader() {
        return pageHeader;
    }

    public void setPageHeader(String pageHeader) {
        this.pageHeader = pageHeader;
    }

    public String getPageFooter() {
        return pageFooter;
    }

    public void setPageFooter(String pageFooter) {
        this.pageFooter = pageFooter;
    }

    public String getQualityBadgeText() {
        return qualityBadgeText;
    }

    public void setQualityBadgeText(String qualityBadgeText) {
        this.qualityBadgeText = qualityBadgeText;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TemplateDTO)) {
            return false;
        }

        TemplateDTO templateDTO = (TemplateDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, templateDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TemplateDTO{" +
            "id=" + getId() +
            ", pageHeader='" + getPageHeader() + "'" +
            ", pageFooter='" + getPageFooter() + "'" +
            ", qualityBadgeText='" + getQualityBadgeText() + "'" +
            "}";
    }
}
