package ee.foodbank.volunteerregistration.service;

import ee.foodbank.volunteerregistration.domain.Volunteer;
import ee.foodbank.volunteerregistration.repository.VolunteerRepository;
import ee.foodbank.volunteerregistration.service.dto.VolunteerDTO;
import ee.foodbank.volunteerregistration.service.mapper.VolunteerMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Volunteer}.
 */
@Service
@Transactional
public class VolunteerService {

    private final Logger log = LoggerFactory.getLogger(VolunteerService.class);

    private final VolunteerRepository volunteerRepository;

    private final VolunteerMapper volunteerMapper;

    public VolunteerService(VolunteerRepository volunteerRepository, VolunteerMapper volunteerMapper) {
        this.volunteerRepository = volunteerRepository;
        this.volunteerMapper = volunteerMapper;
    }

    /**
     * Save a volunteer.
     *
     * @param volunteerDTO the entity to save.
     * @return the persisted entity.
     */
    public VolunteerDTO save(VolunteerDTO volunteerDTO) {
        log.debug("Request to save Volunteer : {}", volunteerDTO);
        Volunteer volunteer = volunteerMapper.toEntity(volunteerDTO);
        volunteer = volunteerRepository.save(volunteer);
        return volunteerMapper.toDto(volunteer);
    }

    /**
     * Partially update a volunteer.
     *
     * @param volunteerDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<VolunteerDTO> partialUpdate(VolunteerDTO volunteerDTO) {
        log.debug("Request to partially update Volunteer : {}", volunteerDTO);

        return volunteerRepository
            .findById(volunteerDTO.getId())
            .map(
                existingVolunteer -> {
                    if (volunteerDTO.getFirstName() != null) {
                        existingVolunteer.setFirstName(volunteerDTO.getFirstName());
                    }

                    if (volunteerDTO.getLastName() != null) {
                        existingVolunteer.setLastName(volunteerDTO.getLastName());
                    }

                    if (volunteerDTO.getAge() != null) {
                        existingVolunteer.setAge(volunteerDTO.getAge());
                    }

                    if (volunteerDTO.getPhone() != null) {
                        existingVolunteer.setPhone(volunteerDTO.getPhone());
                    }

                    if (volunteerDTO.getEmail() != null) {
                        existingVolunteer.setEmail(volunteerDTO.getEmail());
                    }

                    if (volunteerDTO.getIsGroup() != null) {
                        existingVolunteer.setIsGroup(volunteerDTO.getIsGroup());
                    }

                    if (volunteerDTO.getGroupName() != null) {
                        existingVolunteer.setGroupName(volunteerDTO.getGroupName());
                    }

                    if (volunteerDTO.getGroupParticipantCount() != null) {
                        existingVolunteer.setGroupParticipantCount(volunteerDTO.getGroupParticipantCount());
                    }

                    if (volunteerDTO.getNotes() != null) {
                        existingVolunteer.setNotes(volunteerDTO.getNotes());
                    }

                    return existingVolunteer;
                }
            )
            .map(volunteerRepository::save)
            .map(volunteerMapper::toDto);
    }

    /**
     * Get all the volunteers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<VolunteerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Volunteers");
        return volunteerRepository.findAll(pageable).map(volunteerMapper::toDto);
    }

    /**
     * Get one volunteer by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<VolunteerDTO> findOne(Long id) {
        log.debug("Request to get Volunteer : {}", id);
        return volunteerRepository.findById(id).map(volunteerMapper::toDto);
    }

    /**
     * Delete the volunteer by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Volunteer : {}", id);
        volunteerRepository.deleteById(id);
    }
}
