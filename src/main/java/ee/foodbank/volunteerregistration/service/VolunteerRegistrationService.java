package ee.foodbank.volunteerregistration.service;

import ee.foodbank.volunteerregistration.domain.VolunteerRegistration;
import ee.foodbank.volunteerregistration.repository.VolunteerRegistrationRepository;
import ee.foodbank.volunteerregistration.service.dto.VolunteerRegistrationDTO;
import ee.foodbank.volunteerregistration.service.mapper.VolunteerRegistrationMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link VolunteerRegistration}.
 */
@Service
@Transactional
public class VolunteerRegistrationService {

    private final Logger log = LoggerFactory.getLogger(VolunteerRegistrationService.class);

    private final VolunteerRegistrationRepository volunteerRegistrationRepository;

    private final VolunteerRegistrationMapper volunteerRegistrationMapper;

    public VolunteerRegistrationService(
        VolunteerRegistrationRepository volunteerRegistrationRepository,
        VolunteerRegistrationMapper volunteerRegistrationMapper
    ) {
        this.volunteerRegistrationRepository = volunteerRegistrationRepository;
        this.volunteerRegistrationMapper = volunteerRegistrationMapper;
    }

    /**
     * Save a volunteerRegistration.
     *
     * @param volunteerRegistrationDTO the entity to save.
     * @return the persisted entity.
     */
    public VolunteerRegistrationDTO save(VolunteerRegistrationDTO volunteerRegistrationDTO) {
        log.debug("Request to save VolunteerRegistration : {}", volunteerRegistrationDTO);
        VolunteerRegistration volunteerRegistration = volunteerRegistrationMapper.toEntity(volunteerRegistrationDTO);
        volunteerRegistration = volunteerRegistrationRepository.save(volunteerRegistration);
        return volunteerRegistrationMapper.toDto(volunteerRegistration);
    }

    /**
     * Partially update a volunteerRegistration.
     *
     * @param volunteerRegistrationDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<VolunteerRegistrationDTO> partialUpdate(VolunteerRegistrationDTO volunteerRegistrationDTO) {
        log.debug("Request to partially update VolunteerRegistration : {}", volunteerRegistrationDTO);

        return volunteerRegistrationRepository
            .findById(volunteerRegistrationDTO.getId())
            .map(
                existingVolunteerRegistration -> {
                    if (volunteerRegistrationDTO.getRegistrationTime() != null) {
                        existingVolunteerRegistration.setRegistrationTime(volunteerRegistrationDTO.getRegistrationTime());
                    }

                    return existingVolunteerRegistration;
                }
            )
            .map(volunteerRegistrationRepository::save)
            .map(volunteerRegistrationMapper::toDto);
    }

    /**
     * Get all the volunteerRegistrations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<VolunteerRegistrationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all VolunteerRegistrations");
        return volunteerRegistrationRepository.findAll(pageable).map(volunteerRegistrationMapper::toDto);
    }

    /**
     * Get one volunteerRegistration by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<VolunteerRegistrationDTO> findOne(Long id) {
        log.debug("Request to get VolunteerRegistration : {}", id);
        return volunteerRegistrationRepository.findById(id).map(volunteerRegistrationMapper::toDto);
    }

    /**
     * Delete the volunteerRegistration by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete VolunteerRegistration : {}", id);
        volunteerRegistrationRepository.deleteById(id);
    }
}
