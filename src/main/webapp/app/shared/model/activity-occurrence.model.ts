import { IActivity } from '@/shared/model/activity.model';
import { ILocation } from '@/shared/model/location.model';

export interface IActivityOccurrence {
  id?: number;
  startTime?: Date;
  endTime?: Date;
  maxVolunteers?: number;
  registeredVolunteers?: number;
  activity?: IActivity;
  location?: ILocation;
}

export class ActivityOccurrence implements IActivityOccurrence {
  constructor(
    public id?: number,
    public startTime?: Date,
    public endTime?: Date,
    public maxVolunteers?: number,
    public registeredVolunteers?: number,
    public activity?: IActivity,
    public location?: ILocation
  ) {}
}
