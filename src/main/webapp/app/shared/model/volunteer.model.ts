export interface IVolunteer {
  id?: number;
  firstName?: string;
  lastName?: string;
  age?: number;
  phone?: string;
  email?: string;
  isGroup?: boolean;
  groupName?: string | null;
  groupParticipantCount?: number | null;
  notes?: string | null;
}

export class Volunteer implements IVolunteer {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public age?: number,
    public phone?: string,
    public email?: string,
    public isGroup?: boolean,
    public groupName?: string | null,
    public groupParticipantCount?: number | null,
    public notes?: string | null
  ) {
    this.isGroup = this.isGroup ?? false;
  }
}
