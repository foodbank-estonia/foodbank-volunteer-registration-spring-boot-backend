export interface IFoodbankRegion {
  id?: number;
  name?: string;
  description?: string;
  qualityBadge?: boolean | null;
}

export class FoodbankRegion implements IFoodbankRegion {
  constructor(public id?: number, public name?: string, public description?: string, public qualityBadge?: boolean | null) {
    this.qualityBadge = this.qualityBadge ?? false;
  }
}
