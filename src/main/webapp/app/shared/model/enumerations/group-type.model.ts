export enum GroupType {
  ONE = 'ONE',

  MORE_THAN_ONE = 'MORE_THAN_ONE',

  BOTH = 'BOTH',
}
