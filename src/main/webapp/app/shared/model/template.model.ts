export interface ITemplate {
  id?: number;
  pageHeader?: string;
  pageFooter?: string;
  qualityBadgeText?: string;
}

export class Template implements ITemplate {
  constructor(public id?: number, public pageHeader?: string, public pageFooter?: string, public qualityBadgeText?: string) {}
}
