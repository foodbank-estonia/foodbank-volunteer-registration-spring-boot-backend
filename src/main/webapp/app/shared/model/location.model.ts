import { IFoodbankRegion } from '@/shared/model/foodbank-region.model';

export interface ILocation {
  id?: number;
  name?: string;
  address?: string;
  longitude?: number | null;
  latitude?: number | null;
  foodbankRegion?: IFoodbankRegion;
}

export class Location implements ILocation {
  constructor(
    public id?: number,
    public name?: string,
    public address?: string,
    public longitude?: number | null,
    public latitude?: number | null,
    public foodbankRegion?: IFoodbankRegion
  ) {}
}
