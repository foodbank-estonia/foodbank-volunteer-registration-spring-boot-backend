import { IFoodbankRegion } from '@/shared/model/foodbank-region.model';

import { GroupType } from '@/shared/model/enumerations/group-type.model';
export interface IActivity {
  id?: number;
  group?: GroupType;
  name?: string;
  description?: string;
  pictureContentType?: string | null;
  picture?: string | null;
  foodbankRegion?: IFoodbankRegion;
}

export class Activity implements IActivity {
  constructor(
    public id?: number,
    public group?: GroupType,
    public name?: string,
    public description?: string,
    public pictureContentType?: string | null,
    public picture?: string | null,
    public foodbankRegion?: IFoodbankRegion
  ) {}
}
