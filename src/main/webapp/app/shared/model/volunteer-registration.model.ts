import { IActivityOccurrence } from '@/shared/model/activity-occurrence.model';
import { IVolunteer } from '@/shared/model/volunteer.model';

export interface IVolunteerRegistration {
  id?: number;
  registrationTime?: Date;
  activityOccurrence?: IActivityOccurrence;
  volunteer?: IVolunteer;
}

export class VolunteerRegistration implements IVolunteerRegistration {
  constructor(
    public id?: number,
    public registrationTime?: Date,
    public activityOccurrence?: IActivityOccurrence,
    public volunteer?: IVolunteer
  ) {}
}
