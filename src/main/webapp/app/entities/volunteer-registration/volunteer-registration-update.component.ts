import { Component, Vue, Inject } from 'vue-property-decorator';

import { required } from 'vuelidate/lib/validators';
import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import ActivityOccurrenceService from '@/entities/activity-occurrence/activity-occurrence.service';
import { IActivityOccurrence } from '@/shared/model/activity-occurrence.model';

import VolunteerService from '@/entities/volunteer/volunteer.service';
import { IVolunteer } from '@/shared/model/volunteer.model';

import { IVolunteerRegistration, VolunteerRegistration } from '@/shared/model/volunteer-registration.model';
import VolunteerRegistrationService from './volunteer-registration.service';

const validations: any = {
  volunteerRegistration: {
    registrationTime: {
      required,
    },
    activityOccurrence: {
      required,
    },
    volunteer: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class VolunteerRegistrationUpdate extends Vue {
  @Inject('volunteerRegistrationService') private volunteerRegistrationService: () => VolunteerRegistrationService;
  public volunteerRegistration: IVolunteerRegistration = new VolunteerRegistration();

  @Inject('activityOccurrenceService') private activityOccurrenceService: () => ActivityOccurrenceService;

  public activityOccurrences: IActivityOccurrence[] = [];

  @Inject('volunteerService') private volunteerService: () => VolunteerService;

  public volunteers: IVolunteer[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.volunteerRegistrationId) {
        vm.retrieveVolunteerRegistration(to.params.volunteerRegistrationId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.volunteerRegistration.id) {
      this.volunteerRegistrationService()
        .update(this.volunteerRegistration)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('foodbankVolunteerRegistrationApp.volunteerRegistration.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    } else {
      this.volunteerRegistrationService()
        .create(this.volunteerRegistration)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('foodbankVolunteerRegistrationApp.volunteerRegistration.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.volunteerRegistration[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.volunteerRegistration[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.volunteerRegistration[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.volunteerRegistration[field] = null;
    }
  }

  public retrieveVolunteerRegistration(volunteerRegistrationId): void {
    this.volunteerRegistrationService()
      .find(volunteerRegistrationId)
      .then(res => {
        res.registrationTime = new Date(res.registrationTime);
        this.volunteerRegistration = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.activityOccurrenceService()
      .retrieve()
      .then(res => {
        this.activityOccurrences = res.data;
      });
    this.volunteerService()
      .retrieve()
      .then(res => {
        this.volunteers = res.data;
      });
  }
}
