import { Component, Vue, Inject } from 'vue-property-decorator';

import { IVolunteerRegistration } from '@/shared/model/volunteer-registration.model';
import VolunteerRegistrationService from './volunteer-registration.service';

@Component
export default class VolunteerRegistrationDetails extends Vue {
  @Inject('volunteerRegistrationService') private volunteerRegistrationService: () => VolunteerRegistrationService;
  public volunteerRegistration: IVolunteerRegistration = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.volunteerRegistrationId) {
        vm.retrieveVolunteerRegistration(to.params.volunteerRegistrationId);
      }
    });
  }

  public retrieveVolunteerRegistration(volunteerRegistrationId) {
    this.volunteerRegistrationService()
      .find(volunteerRegistrationId)
      .then(res => {
        this.volunteerRegistration = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
