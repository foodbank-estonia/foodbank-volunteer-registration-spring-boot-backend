import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { IVolunteer } from '@/shared/model/volunteer.model';
import VolunteerService from './volunteer.service';

@Component
export default class VolunteerDetails extends mixins(JhiDataUtils) {
  @Inject('volunteerService') private volunteerService: () => VolunteerService;
  public volunteer: IVolunteer = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.volunteerId) {
        vm.retrieveVolunteer(to.params.volunteerId);
      }
    });
  }

  public retrieveVolunteer(volunteerId) {
    this.volunteerService()
      .find(volunteerId)
      .then(res => {
        this.volunteer = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
