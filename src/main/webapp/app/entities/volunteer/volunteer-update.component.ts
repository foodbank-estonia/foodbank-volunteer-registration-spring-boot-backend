import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { required, minLength, maxLength, numeric, minValue, maxValue } from 'vuelidate/lib/validators';

import { IVolunteer, Volunteer } from '@/shared/model/volunteer.model';
import VolunteerService from './volunteer.service';

const validations: any = {
  volunteer: {
    firstName: {
      required,
      minLength: minLength(2),
      maxLength: maxLength(100),
    },
    lastName: {
      required,
      minLength: minLength(2),
      maxLength: maxLength(100),
    },
    age: {
      required,
      numeric,
      min: minValue(5),
      max: maxValue(100),
    },
    phone: {
      required,
      minLength: minLength(4),
      maxLength: maxLength(21),
    },
    email: {
      required,
      minLength: minLength(6),
      maxLength: maxLength(254),
    },
    isGroup: {
      required,
    },
    groupName: {
      minLength: minLength(2),
      maxLength: maxLength(100),
    },
    groupParticipantCount: {
      numeric,
      min: minValue(2),
      max: maxValue(50),
    },
    notes: {},
  },
};

@Component({
  validations,
})
export default class VolunteerUpdate extends mixins(JhiDataUtils) {
  @Inject('volunteerService') private volunteerService: () => VolunteerService;
  public volunteer: IVolunteer = new Volunteer();
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.volunteerId) {
        vm.retrieveVolunteer(to.params.volunteerId);
      }
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.volunteer.id) {
      this.volunteerService()
        .update(this.volunteer)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('foodbankVolunteerRegistrationApp.volunteer.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    } else {
      this.volunteerService()
        .create(this.volunteer)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('foodbankVolunteerRegistrationApp.volunteer.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    }
  }

  public retrieveVolunteer(volunteerId): void {
    this.volunteerService()
      .find(volunteerId)
      .then(res => {
        this.volunteer = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
