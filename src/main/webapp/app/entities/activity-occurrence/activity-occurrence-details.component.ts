import { Component, Vue, Inject } from 'vue-property-decorator';

import { IActivityOccurrence } from '@/shared/model/activity-occurrence.model';
import ActivityOccurrenceService from './activity-occurrence.service';

@Component
export default class ActivityOccurrenceDetails extends Vue {
  @Inject('activityOccurrenceService') private activityOccurrenceService: () => ActivityOccurrenceService;
  public activityOccurrence: IActivityOccurrence = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.activityOccurrenceId) {
        vm.retrieveActivityOccurrence(to.params.activityOccurrenceId);
      }
    });
  }

  public retrieveActivityOccurrence(activityOccurrenceId) {
    this.activityOccurrenceService()
      .find(activityOccurrenceId)
      .then(res => {
        this.activityOccurrence = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
