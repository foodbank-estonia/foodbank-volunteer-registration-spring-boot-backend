import { mixins } from 'vue-class-component';

import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IActivityOccurrence } from '@/shared/model/activity-occurrence.model';

import ActivityOccurrenceService from './activity-occurrence.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class ActivityOccurrence extends Vue {
  @Inject('activityOccurrenceService') private activityOccurrenceService: () => ActivityOccurrenceService;
  private removeId: number = null;
  public itemsPerPage = 20;
  public queryCount: number = null;
  public page = 1;
  public previousPage = 1;
  public propOrder = 'id';
  public reverse = false;
  public totalItems = 0;

  public activityOccurrences: IActivityOccurrence[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllActivityOccurrences();
  }

  public clear(): void {
    this.page = 1;
    this.retrieveAllActivityOccurrences();
  }

  public retrieveAllActivityOccurrences(): void {
    this.isFetching = true;

    const paginationQuery = {
      page: this.page - 1,
      size: this.itemsPerPage,
      sort: this.sort(),
    };
    this.activityOccurrenceService()
      .retrieve(paginationQuery)
      .then(
        res => {
          this.activityOccurrences = res.data;
          this.totalItems = Number(res.headers['x-total-count']);
          this.queryCount = this.totalItems;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public handleSyncList(): void {
    this.clear();
  }

  public prepareRemove(instance: IActivityOccurrence): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeActivityOccurrence(): void {
    this.activityOccurrenceService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('foodbankVolunteerRegistrationApp.activityOccurrence.deleted', { param: this.removeId });
        this.$bvToast.toast(message.toString(), {
          toaster: 'b-toaster-top-center',
          title: 'Info',
          variant: 'danger',
          solid: true,
          autoHideDelay: 5000,
        });
        this.removeId = null;
        this.retrieveAllActivityOccurrences();
        this.closeDialog();
      });
  }

  public sort(): Array<any> {
    const result = [this.propOrder + ',' + (this.reverse ? 'desc' : 'asc')];
    if (this.propOrder !== 'id') {
      result.push('id');
    }
    return result;
  }

  public loadPage(page: number): void {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  public transition(): void {
    this.retrieveAllActivityOccurrences();
  }

  public changeOrder(propOrder): void {
    this.propOrder = propOrder;
    this.reverse = !this.reverse;
    this.transition();
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
