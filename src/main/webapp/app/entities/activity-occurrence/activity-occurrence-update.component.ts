import { Component, Vue, Inject } from 'vue-property-decorator';

import { required, numeric, minValue, maxValue } from 'vuelidate/lib/validators';
import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import ActivityService from '@/entities/activity/activity.service';
import { IActivity } from '@/shared/model/activity.model';

import LocationService from '@/entities/location/location.service';
import { ILocation } from '@/shared/model/location.model';

import { IActivityOccurrence, ActivityOccurrence } from '@/shared/model/activity-occurrence.model';
import ActivityOccurrenceService from './activity-occurrence.service';

const validations: any = {
  activityOccurrence: {
    startTime: {
      required,
    },
    endTime: {
      required,
    },
    maxVolunteers: {
      required,
      numeric,
      min: minValue(1),
      max: maxValue(50),
    },
    registeredVolunteers: {
      required,
      numeric,
      min: minValue(0),
      max: maxValue(50),
    },
    activity: {
      required,
    },
    location: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class ActivityOccurrenceUpdate extends Vue {
  @Inject('activityOccurrenceService') private activityOccurrenceService: () => ActivityOccurrenceService;
  public activityOccurrence: IActivityOccurrence = new ActivityOccurrence();

  @Inject('activityService') private activityService: () => ActivityService;

  public activities: IActivity[] = [];

  @Inject('locationService') private locationService: () => LocationService;

  public locations: ILocation[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.activityOccurrenceId) {
        vm.retrieveActivityOccurrence(to.params.activityOccurrenceId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.activityOccurrence.id) {
      this.activityOccurrenceService()
        .update(this.activityOccurrence)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('foodbankVolunteerRegistrationApp.activityOccurrence.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    } else {
      this.activityOccurrenceService()
        .create(this.activityOccurrence)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('foodbankVolunteerRegistrationApp.activityOccurrence.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.activityOccurrence[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.activityOccurrence[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.activityOccurrence[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.activityOccurrence[field] = null;
    }
  }

  public retrieveActivityOccurrence(activityOccurrenceId): void {
    this.activityOccurrenceService()
      .find(activityOccurrenceId)
      .then(res => {
        res.startTime = new Date(res.startTime);
        res.endTime = new Date(res.endTime);
        this.activityOccurrence = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.activityService()
      .retrieve()
      .then(res => {
        this.activities = res.data;
      });
    this.locationService()
      .retrieve()
      .then(res => {
        this.locations = res.data;
      });
  }
}
