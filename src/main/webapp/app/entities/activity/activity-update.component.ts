import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { required, minLength, maxLength } from 'vuelidate/lib/validators';

import FoodbankRegionService from '@/entities/foodbank-region/foodbank-region.service';
import { IFoodbankRegion } from '@/shared/model/foodbank-region.model';

import { IActivity, Activity } from '@/shared/model/activity.model';
import ActivityService from './activity.service';

const validations: any = {
  activity: {
    group: {
      required,
    },
    name: {
      required,
      minLength: minLength(2),
      maxLength: maxLength(255),
    },
    description: {
      required,
    },
    picture: {},
    foodbankRegion: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class ActivityUpdate extends mixins(JhiDataUtils) {
  @Inject('activityService') private activityService: () => ActivityService;
  public activity: IActivity = new Activity();

  @Inject('foodbankRegionService') private foodbankRegionService: () => FoodbankRegionService;

  public foodbankRegions: IFoodbankRegion[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.activityId) {
        vm.retrieveActivity(to.params.activityId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.activity.id) {
      this.activityService()
        .update(this.activity)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('foodbankVolunteerRegistrationApp.activity.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    } else {
      this.activityService()
        .create(this.activity)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('foodbankVolunteerRegistrationApp.activity.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    }
  }

  public retrieveActivity(activityId): void {
    this.activityService()
      .find(activityId)
      .then(res => {
        this.activity = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public clearInputImage(field, fieldContentType, idInput): void {
    if (this.activity && field && fieldContentType) {
      if (Object.prototype.hasOwnProperty.call(this.activity, field)) {
        this.activity[field] = null;
      }
      if (Object.prototype.hasOwnProperty.call(this.activity, fieldContentType)) {
        this.activity[fieldContentType] = null;
      }
      if (idInput) {
        (<any>this).$refs[idInput] = null;
      }
    }
  }

  public initRelationships(): void {
    this.foodbankRegionService()
      .retrieve()
      .then(res => {
        this.foodbankRegions = res.data;
      });
  }
}
