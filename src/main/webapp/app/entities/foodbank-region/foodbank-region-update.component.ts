import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { required, minLength, maxLength } from 'vuelidate/lib/validators';

import { IFoodbankRegion, FoodbankRegion } from '@/shared/model/foodbank-region.model';
import FoodbankRegionService from './foodbank-region.service';

const validations: any = {
  foodbankRegion: {
    name: {
      required,
      minLength: minLength(2),
      maxLength: maxLength(255),
    },
    description: {
      required,
    },
    qualityBadge: {},
  },
};

@Component({
  validations,
})
export default class FoodbankRegionUpdate extends mixins(JhiDataUtils) {
  @Inject('foodbankRegionService') private foodbankRegionService: () => FoodbankRegionService;
  public foodbankRegion: IFoodbankRegion = new FoodbankRegion();
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.foodbankRegionId) {
        vm.retrieveFoodbankRegion(to.params.foodbankRegionId);
      }
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.foodbankRegion.id) {
      this.foodbankRegionService()
        .update(this.foodbankRegion)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('foodbankVolunteerRegistrationApp.foodbankRegion.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    } else {
      this.foodbankRegionService()
        .create(this.foodbankRegion)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('foodbankVolunteerRegistrationApp.foodbankRegion.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    }
  }

  public retrieveFoodbankRegion(foodbankRegionId): void {
    this.foodbankRegionService()
      .find(foodbankRegionId)
      .then(res => {
        this.foodbankRegion = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
