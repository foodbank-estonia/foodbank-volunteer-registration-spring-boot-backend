import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { IFoodbankRegion } from '@/shared/model/foodbank-region.model';
import FoodbankRegionService from './foodbank-region.service';

@Component
export default class FoodbankRegionDetails extends mixins(JhiDataUtils) {
  @Inject('foodbankRegionService') private foodbankRegionService: () => FoodbankRegionService;
  public foodbankRegion: IFoodbankRegion = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.foodbankRegionId) {
        vm.retrieveFoodbankRegion(to.params.foodbankRegionId);
      }
    });
  }

  public retrieveFoodbankRegion(foodbankRegionId) {
    this.foodbankRegionService()
      .find(foodbankRegionId)
      .then(res => {
        this.foodbankRegion = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
