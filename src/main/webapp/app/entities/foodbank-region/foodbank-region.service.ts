import axios from 'axios';

import buildPaginationQueryOpts from '@/shared/sort/sorts';

import { IFoodbankRegion } from '@/shared/model/foodbank-region.model';

const baseApiUrl = 'api/foodbank-regions';

export default class FoodbankRegionService {
  public find(id: number): Promise<IFoodbankRegion> {
    return new Promise<IFoodbankRegion>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieve(paginationQuery?: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(baseApiUrl + `?${buildPaginationQueryOpts(paginationQuery)}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public delete(id: number): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .delete(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public create(entity: IFoodbankRegion): Promise<IFoodbankRegion> {
    return new Promise<IFoodbankRegion>((resolve, reject) => {
      axios
        .post(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public update(entity: IFoodbankRegion): Promise<IFoodbankRegion> {
    return new Promise<IFoodbankRegion>((resolve, reject) => {
      axios
        .put(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}
