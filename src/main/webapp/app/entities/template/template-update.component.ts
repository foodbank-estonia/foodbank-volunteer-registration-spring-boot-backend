import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { required } from 'vuelidate/lib/validators';

import { ITemplate, Template } from '@/shared/model/template.model';
import TemplateService from './template.service';

const validations: any = {
  template: {
    pageHeader: {
      required,
    },
    pageFooter: {
      required,
    },
    qualityBadgeText: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class TemplateUpdate extends mixins(JhiDataUtils) {
  @Inject('templateService') private templateService: () => TemplateService;
  public template: ITemplate = new Template();
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.templateId) {
        vm.retrieveTemplate(to.params.templateId);
      }
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.template.id) {
      this.templateService()
        .update(this.template)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('foodbankVolunteerRegistrationApp.template.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    } else {
      this.templateService()
        .create(this.template)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('foodbankVolunteerRegistrationApp.template.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    }
  }

  public retrieveTemplate(templateId): void {
    this.templateService()
      .find(templateId)
      .then(res => {
        this.template = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
