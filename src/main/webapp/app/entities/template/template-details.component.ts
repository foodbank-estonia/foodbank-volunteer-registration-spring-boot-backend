import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { ITemplate } from '@/shared/model/template.model';
import TemplateService from './template.service';

@Component
export default class TemplateDetails extends mixins(JhiDataUtils) {
  @Inject('templateService') private templateService: () => TemplateService;
  public template: ITemplate = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.templateId) {
        vm.retrieveTemplate(to.params.templateId);
      }
    });
  }

  public retrieveTemplate(templateId) {
    this.templateService()
      .find(templateId)
      .then(res => {
        this.template = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
