import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore

// prettier-ignore
const FoodbankRegion = () => import('@/entities/foodbank-region/foodbank-region.vue');
// prettier-ignore
const FoodbankRegionUpdate = () => import('@/entities/foodbank-region/foodbank-region-update.vue');
// prettier-ignore
const FoodbankRegionDetails = () => import('@/entities/foodbank-region/foodbank-region-details.vue');
// prettier-ignore
const Activity = () => import('@/entities/activity/activity.vue');
// prettier-ignore
const ActivityUpdate = () => import('@/entities/activity/activity-update.vue');
// prettier-ignore
const ActivityDetails = () => import('@/entities/activity/activity-details.vue');
// prettier-ignore
const ActivityOccurrence = () => import('@/entities/activity-occurrence/activity-occurrence.vue');
// prettier-ignore
const ActivityOccurrenceUpdate = () => import('@/entities/activity-occurrence/activity-occurrence-update.vue');
// prettier-ignore
const ActivityOccurrenceDetails = () => import('@/entities/activity-occurrence/activity-occurrence-details.vue');
// prettier-ignore
const Location = () => import('@/entities/location/location.vue');
// prettier-ignore
const LocationUpdate = () => import('@/entities/location/location-update.vue');
// prettier-ignore
const LocationDetails = () => import('@/entities/location/location-details.vue');
// prettier-ignore
const Volunteer = () => import('@/entities/volunteer/volunteer.vue');
// prettier-ignore
const VolunteerUpdate = () => import('@/entities/volunteer/volunteer-update.vue');
// prettier-ignore
const VolunteerDetails = () => import('@/entities/volunteer/volunteer-details.vue');
// prettier-ignore
const VolunteerRegistration = () => import('@/entities/volunteer-registration/volunteer-registration.vue');
// prettier-ignore
const VolunteerRegistrationUpdate = () => import('@/entities/volunteer-registration/volunteer-registration-update.vue');
// prettier-ignore
const VolunteerRegistrationDetails = () => import('@/entities/volunteer-registration/volunteer-registration-details.vue');
// prettier-ignore
const Template = () => import('@/entities/template/template.vue');
// prettier-ignore
const TemplateUpdate = () => import('@/entities/template/template-update.vue');
// prettier-ignore
const TemplateDetails = () => import('@/entities/template/template-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

export default [
  {
    path: '/foodbank-region',
    name: 'FoodbankRegion',
    component: FoodbankRegion,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/foodbank-region/new',
    name: 'FoodbankRegionCreate',
    component: FoodbankRegionUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/foodbank-region/:foodbankRegionId/edit',
    name: 'FoodbankRegionEdit',
    component: FoodbankRegionUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/foodbank-region/:foodbankRegionId/view',
    name: 'FoodbankRegionView',
    component: FoodbankRegionDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/activity',
    name: 'Activity',
    component: Activity,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/activity/new',
    name: 'ActivityCreate',
    component: ActivityUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/activity/:activityId/edit',
    name: 'ActivityEdit',
    component: ActivityUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/activity/:activityId/view',
    name: 'ActivityView',
    component: ActivityDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/activity-occurrence',
    name: 'ActivityOccurrence',
    component: ActivityOccurrence,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/activity-occurrence/new',
    name: 'ActivityOccurrenceCreate',
    component: ActivityOccurrenceUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/activity-occurrence/:activityOccurrenceId/edit',
    name: 'ActivityOccurrenceEdit',
    component: ActivityOccurrenceUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/activity-occurrence/:activityOccurrenceId/view',
    name: 'ActivityOccurrenceView',
    component: ActivityOccurrenceDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/location',
    name: 'Location',
    component: Location,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/location/new',
    name: 'LocationCreate',
    component: LocationUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/location/:locationId/edit',
    name: 'LocationEdit',
    component: LocationUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/location/:locationId/view',
    name: 'LocationView',
    component: LocationDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/volunteer',
    name: 'Volunteer',
    component: Volunteer,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/volunteer/new',
    name: 'VolunteerCreate',
    component: VolunteerUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/volunteer/:volunteerId/edit',
    name: 'VolunteerEdit',
    component: VolunteerUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/volunteer/:volunteerId/view',
    name: 'VolunteerView',
    component: VolunteerDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/volunteer-registration',
    name: 'VolunteerRegistration',
    component: VolunteerRegistration,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/volunteer-registration/new',
    name: 'VolunteerRegistrationCreate',
    component: VolunteerRegistrationUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/volunteer-registration/:volunteerRegistrationId/edit',
    name: 'VolunteerRegistrationEdit',
    component: VolunteerRegistrationUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/volunteer-registration/:volunteerRegistrationId/view',
    name: 'VolunteerRegistrationView',
    component: VolunteerRegistrationDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/template',
    name: 'Template',
    component: Template,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/template/new',
    name: 'TemplateCreate',
    component: TemplateUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/template/:templateId/edit',
    name: 'TemplateEdit',
    component: TemplateUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/template/:templateId/view',
    name: 'TemplateView',
    component: TemplateDetails,
    meta: { authorities: [Authority.USER] },
  },
  // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
];
